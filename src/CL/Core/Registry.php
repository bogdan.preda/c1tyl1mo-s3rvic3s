<?php

namespace CL\Core;

abstract class Registry {

    private static $items = array();

    public static function set($name, $value) {

        self::$items[$name] = $value;
    }

    public static function get($name) {

        return self::$items[$name];
    }
}
?>