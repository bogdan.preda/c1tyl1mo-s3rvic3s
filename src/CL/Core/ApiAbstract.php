<?php
/**
 * ApiAbstract.php
 *
 * PHP version 5
 *
 * @package   CL\Core
 * @author    "Geana Raul" <raul.geana@digitalxbridge.com>
 * @copyright 2013 DigitalXBridge LLC.
 * @license   Proprietary license.
 */

namespace CL\Core;

use \CL\Core\Exception as BaseException;

/**
 * class ApiAbstract
 *
 * abstract Api Class
 *
 * @author    "Geana Raul" <raul.geana@digitalxbridge.com>
 * @copyright 2013 DigitalXBridge LLC.
 * @license   Proprietary license.
 */
class ApiAbstract
{
    /**
     * @var array
     */
    protected static $instance = null;

    /**
     * Block Construct call
     */
    protected function __construct()
    {
        $trace        = debug_backtrace();
        $previousCall = $trace[2]['function'];

        switch ($previousCall) {
            case 'getInstance' :
                break;
            default :
                throw new BaseException(
                    BaseException::API_CONSTRUCT_TEXT,
                    BaseException::API_CONSTRUCT_CODE);
                break;
        }
    }

    /**
     * Block Clone call
     */
    protected function __clone()
    {
        $trace        = debug_backtrace();
        $previousCall = $trace[2]['function'];

        switch ($previousCall) {
            case 'getInstance' :
                break;
            default :
                throw new BaseException(
                    BaseException::API_CLONE_TEXT,
                    BaseException::API_CLONE_CODE);
                break;
        }
    }

    /**
     * init method
     */
    protected function init()
    {

    }

    /**
     * return a instance of the called object
     *
     * @throws BaseException
     *
     * @return object
     */
    public static function getInstance()
    {
        $class = get_called_class();

        if (!isset(self::$instance[$class])) {
            self::getNewInstance($class);
        }

        return self::$instance[$class];
    }

    /**
     * return a instance of the called object
     *
     * @param string|null $className name of a class
     *
     * @throws BaseException
     *
     * @return object
     */
    public static function getNewInstance($className = null)
    {
        if (is_null($className)) {
            $class = get_called_class();
        } else {
            $class = $className;
        }

        try {

            self::$instance[$class] = new static;
            self::$instance[$class]->init();

        } catch (\Exception $e) {

            throw new BaseException(
                $e->getMessage(),
                $e->getCode());
        }

        return self::$instance[$class];
    }
}
?>