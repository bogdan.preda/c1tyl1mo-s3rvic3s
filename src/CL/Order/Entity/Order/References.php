<?php

namespace CL\Order\Entity\Order;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity,
    CL\Booking\Mapper\Booking as BookingMapper,
    CL\Company\Mapper\Company as CompanyMapper,
    CL\User\Mapper\User as UserMapper;

class References extends AbstractEntity {

    public function initFields()
    {
        return array(
            'booking' => $this->field()
                    ->setReference(new BookingMapper())
                    ->setRequired(true),
            'payment' => $this->field()
                    /** TODO set reference for payment */
                    ->setRequired(false)
                    ->setIgnore(true),
            'user'    => $this->field()
                    ->setReference(new UserMapper())
                    ->setRequired(false)
                    ->setIgnore(true),
            'company'    => $this->field()
                    ->setReference(new CompanyMapper())
                    ->setRequired(false)
                    ->setIgnore(true)
        );
    }
}
?>