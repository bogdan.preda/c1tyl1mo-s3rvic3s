<?php

namespace CL\Order\Entity\Order;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class ContactInformation extends AbstractEntity {

    public function initFields()
    {
        return array(
            'firstName' => $this->field()->setRequired(true),
            'lastName'  => $this->field()->setRequired(true),
            'fullName'  => $this->field()->setRequired(false),
            'address'   => $this->field()->setRequired(false),
            'email'     => $this->field()->setRequired(true),
            'phone'     => $this->field()->setRequired(true)
        );
    }
}
?>
