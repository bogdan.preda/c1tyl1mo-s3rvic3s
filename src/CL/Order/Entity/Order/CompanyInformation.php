<?php

namespace CL\Order\Entity\Order;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class CompanyInformation extends AbstractEntity {

    public function initFields()
    {
        return array(
            "name"    => $this->field()
                    ->setRequired(true),
            "address" => $this->field()
                    ->setRequired(false),
            "CUI"     => $this->field()
                    ->setRequired(false),
            "J"       => $this->field()
                    ->setRequired(false),
            "phone"   => $this->field()
                    ->setRequired(false)
                    ->setIgnore(false),
            "fax"     => $this->field()
                    ->setRequired(false)
                    ->setIgnore(true),
            "departmentInformation" => $this->field()
                    ->setType('CL\Order\Entity\Order\DepartmentInformation')
                    ->setRequired(false)
                    ->setIgnore(true)
        );
    }
}
?>
