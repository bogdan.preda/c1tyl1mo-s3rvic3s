<?php

namespace CL\Order\Entity\Order;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class DepartmentInformation extends AbstractEntity {

    public function initFields()
    {
        return array(
            "id"   => $this->field()
                    ->setRequired(true),
            "name" => $this->field()
                    ->setRequired(true)
        );
    }
}
?>