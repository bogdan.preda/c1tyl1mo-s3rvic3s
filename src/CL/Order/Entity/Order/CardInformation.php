<?php

namespace CL\Order\Entity\Order;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class CardInformation extends AbstractEntity {

    public function initFields() {

        return array(
            'expirationYear'  => $this->field()->setRequired(true),
            'expirationMonth' => $this->field()->setRequired(true),
            'name'            => $this->field()->setRequired(true),
            'number'          => $this->field()->setRequired(true),
            'CVN'             => $this->field()->setRequired(true),
        );
    }
}