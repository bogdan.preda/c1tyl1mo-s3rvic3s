<?php

namespace CL\Order\Entity;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity,
    \CL\Order\Mapper\Order as OrderMapper;

class Order extends AbstractEntity {

    /**
     * financial status - default status STATUS_PENDING
     */
    const STATUS_PENDING  = 'In asteptare';
    const STATUS_CONFIRM  = 'Confirmat';
    const STATUS_CANCEL   = 'Anulat';
    const STATUS_FINALISE = 'Finalizat';
    const STATUS_BILLED   = 'Facturat';
    const STATUS_PAYED    = 'Achitat';

    public function initFields() {
        return array(
            "__v"                => $this->field()
                    ->setIgnore(true),
            "_id"                => $this->field()
                    ->setIgnore(true),
            "uniqueId"           => $this->field()
                    ->setIgnore(true)
                    ->setValue(0),
            "references"         => $this->field()
                    ->setRequired(true)
                    ->setType('CL\Order\Entity\Order\References'),
            "observations"       => $this->field()
                    ->setRequired(false),            
            "cardInformation"    => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false)
                    ->setType('CL\Order\Entity\Order\CardInformation'),
            "companyInformation" => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false)
                    ->setType('CL\Order\Entity\Order\CompanyInformation'),
            "contactInformation" => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false)
                    ->setType('CL\Order\Entity\Order\ContactInformation'),
            "status"             => $this->field()
                    ->setRequired(false)
                    ->setValue(self::STATUS_PENDING),
            "createdAt"          => $this->field()
                    ->setRequired(true)
        );
    }

    public function preSaveHook() {

        if ($this->getField('uniqueId') == 0) {
            $sort             = array('uniqueId' => -1);
            $orderCollections = OrderMapper::find(null, 1, 0, $sort)->getEntities();

            if (!empty($orderCollections)) {

                $lastInsertedEntity = $orderCollections[0];
                $uniqueId           = $lastInsertedEntity->getField('uniqueId');

                    $this->setField('uniqueId', intval($uniqueId) + 1);
            } else {
                $this->setField('uniqueId', 1);
            }
        }
    }
}
?>