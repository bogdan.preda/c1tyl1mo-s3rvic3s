<?php

namespace CL\Order\Mapper;

use \Xeeo\Services\Database\Mongo\Mapper as Mapper;

class Order extends Mapper {

    protected static $collection = 'orders';
    protected static $entity     = '\CL\Order\Entity\Order';

}
?>