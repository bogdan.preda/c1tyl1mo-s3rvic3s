<?php

namespace CL\Order;

use \CL\Core\ApiAbstract,
    \CL\Order\Entity\Order as OrderEntity,
    \CL\Order\Entity\Order\CompanyInformation as OrderCompanyInfoEntity,
    \CL\Order\Entity\Order\DepartmentInformation as OrderCompanyDepartmentInfoEntity,
    \CL\Company\Entity\Company as CompanyEntity,
    \CL\Order\Mapper\Order as OrderMapper,
    \CL\Config\Entity\Config as ConfigEntity,
    \CL\Config\Mapper\Config as ConfigMapper,
    \CL\Booking\Mapper\Booking as BookingMapper,
    \CL\User\Mapper\User as UserMapper,
    \CL\Car\Api as CarApi,
    \Xeeo\Services\Database\Mongo\Filter;

class Api extends ApiAbstract {

    const ITEMS_PER_PAGE = 10;

    const SORT_DESCENDING = 'DESC';
    const SORT_ASCENDING  = 'ASC';

    /* @var CarApi */
    private $carApi;

    public function init() {

        parent::init();

        $this->carApi = CarApi::getInstance();
    }

    /**
     * Get orders by user id
     *
     * @param \MongoId $userId
     *
     * @return OrderEntity $orders
     */
    public function getOrdersByUserId($userId) {

        $userIdFilter = Filter::set('references.user.$id', Filter::EQUAL, $userId);

        $orders = OrderMapper::find($userIdFilter, 0);

        return $orders;
    }


    /**
     * Get number of orders by department id
     *
     * @param $departmentId
     *
     * @return mixed
     */
    public function getNumberOfOrdersByDepartmentId($departmentId) {

        $departmentIdFilter = Filter::set('companyInformation.departmentInformation.id', Filter::EQUAL, $departmentId);

        $numberOfOrders = OrderMapper::count($departmentIdFilter);

        return $numberOfOrders;
    }

    /**
     * Get total amount from orders by department id
     *
     * @param $departmentId
     *
     * @return int
     */
    public function getAmountOfOrdersByDepartmentId($departmentId) {

        $departmentIdFilter = Filter::set('companyInformation.departmentInformation.id', Filter::EQUAL, $departmentId);

        $orders = OrderMapper::find($departmentIdFilter, 0);

        $totalAmount = 0;
        foreach ($orders as $orderDetails) {

            $bookingEntity = $orderDetails->getField('references')->getField('booking');
            if ($bookingEntity->getField('selectedCarCombination')->getField('priceWithDiscount')) {
                $totalAmount += $bookingEntity->getField('selectedCarCombination')->getField('priceWithDiscount');
            } else {
                $totalAmount += $bookingEntity->getField('selectedCarCombination')->getField('price');
            }

        }

        return $totalAmount;
    }

    public function getOrdersBy($criteria = null, $limit = 10, $offset = 0, array $sort = array()) {

        $orders = OrderMapper::find($criteria, $limit, $offset, $sort);

        return $orders;
    }

    public function convertDateToTimestamp($schedule) {

        list($day, $month, $year) = explode(".", $schedule->getField('date'));

        $timestamp = mktime($schedule->getField('hour'), $schedule->getField('minute'), 0, $month, $day, $year);

        return $timestamp;
    }

    /**
     * Returns collection of orders by a given filter
     *
     * @param array $criteria
     * @param array $sort
     * @param int   $page
     * @param int   $itemsPerPage
     *
     * @return \ArrayObject
     */
    public function findOrdersBy($criteria, $sort = array(), $page = 1, $itemsPerPage = self::ITEMS_PER_PAGE)
    {
        $itemsPerPage = (empty($itemsPerPage)) ? $itemsPerPage : abs((int) $itemsPerPage);
        $page         = (empty($page)) ? 1 : abs((int) $page);
        $offset       = ($page - 1) * $itemsPerPage;

        if (false === is_array($sort)) {
            $sort = array();
        }

        $result = OrderMapper::find($criteria, $itemsPerPage, $offset, $sort);

        return $result;
    }

    public function getOrder($id)
    {
        $result = OrderMapper::get($id);

        return $result;
    }


    public function getConfigEntity() {

        $result = ConfigMapper::findOne();

        return $result;
    }

    /**
     * @param $orderStatus
     *
     * @return array
     */
    public function generateAvailableStatuses($orderStatus) {

        $status = array(
            OrderEntity::STATUS_PENDING => array(
                OrderEntity::STATUS_CONFIRM,
                OrderEntity::STATUS_CANCEL
            ),
            OrderEntity::STATUS_CONFIRM => array(
                OrderEntity::STATUS_FINALISE
            ),
            OrderEntity::STATUS_FINALISE => array(
                OrderEntity::STATUS_BILLED
            ),
            OrderEntity::STATUS_BILLED => array(
                OrderEntity::STATUS_PAYED
            )
        );

        if (isset($status[$orderStatus])) {
            return $status[$orderStatus];
        } else {
            return array();
        }
    }

    public function checkStatus($status) {

        $reflection = new \ReflectionClass('\CL\Order\Entity\Order');
        $constants  = $reflection->getConstants();

        if (!in_array($status, $constants)) {
            return true;
        }
        return false;
    }

    public function generatePdfForOrder($orderId, $language, $downloadPdf = true) {

        // Settings for translate
        if ($language == "en") {
            $languageCode = "en_US.utf-8";
        } else {
            $languageCode = "ro_RO";
            setlocale(LC_TIME,"ro_RO");
        }

        putenv("LANG=" . $languageCode);
        setlocale(LC_ALL, $languageCode);
        bindtextdomain($language, getcwd() . "/languages");
        textdomain($language);

        $orderEntity   = OrderMapper::get($orderId);
        $bookingEntity = $orderEntity->getField('references')->getField('booking');

        $configEntity = $this->getConfigEntity();

        $vatTax =  $configEntity->getPriceRates()->getVatTax(); // $config['priceRates']['vatTax'];

        $userDetails = array(
            'name'    => $orderEntity->getField('contactInformation')->getField('firstName') . ' ' . $orderEntity->getField('contactInformation')->getField('lastName'),
            'email'   => $orderEntity->getField('contactInformation')->getField('email'),
            'address' => $orderEntity->getField('contactInformation')->getField('address'),
            'phone'   => $orderEntity->getField('contactInformation')->getField('phone')
        );

        $origin      = $this->convertText($bookingEntity->getField('origin'));
        $destination = $this->convertText($bookingEntity->getField('destination'));

        if ($bookingEntity->getField('schedule')->getField('type') == 1) {
            $scheduleTypeName = array(
                "data" => getText("Data de ridicare"),
                "ora"  => getText("Ora de ridicare")
            );
        } elseif ($bookingEntity->getField('schedule')->getField('type') == 2) {
            $scheduleTypeName = array(
                "data" => getText("Data de ajuns la destinatie"),
                "ora"  => getText("Ora de ajuns la destinatie")
            );
        }

        if ($bookingEntity->getField('returnSchedule')) {
            if ($bookingEntity->getField('returnSchedule')->getField('type') == 1) {
                $returnScheduleTypeName = array(
                    "data" => getText("Data de ridicare retur"),
                    "ora"  => getText("Ora de ridicare retur")
                );
            } elseif ($bookingEntity->getField('returnSchedule')->getField('type') == 2) {
                $returnScheduleTypeName = array(
                    "data" => getText("Data de ajuns la destinatie retur"),
                    "ora"  => getText("Ora de ajuns la destinatie retur")
                );
            }
        }

        $durationFlag = true;
        if ($bookingEntity->getField('routeType') == 'rent-per-hour') {
            $destinationType  = getText("Durata");
            $destinationValue = $bookingEntity->getField('effort')->getField('duration')/3600 . 'h';
            $durationFlag     = false;
        } else {
            $destinationType  = getText("Pana la");
            $destinationValue =  $destination;
        }

        $pdf   = new \Zend_Pdf();
        $page  = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);
        $page2 = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);

        $logo = \Zend_Pdf_Image::imageWithPath(__DIR__ . '/Resources/logo_print.png');

        $blackStyle = new \Zend_Pdf_Style();
        $blackStyle->setFillColor(new \Zend_Pdf_Color_Html('#000000'));

        $boldLine = new \Zend_Pdf_Style();
        $boldLine->setLineColor(new \Zend_Pdf_Color_Html('#d7d7d7'));
        $boldLine->setFillColor(new \Zend_Pdf_Color_Html('#d7d7d7'));
        $boldLine->setLineWidth(1);

        $pageWidth     = $page->getWidth();
        $pageHeight    = $page->getHeight();
        $pageLeftSpace = 40;

        $imageHeight = 72;
        $imageWidth  = 280;

        $topPos    = $pageHeight - 15;
        $leftPos   = 30;
        $bottomPos = $topPos - $imageHeight;
        $rightPos  = $leftPos + $imageWidth;

        $page->drawImage($logo, $leftPos, $bottomPos, $rightPos, $topPos);
        $page2->drawImage($logo, $leftPos, $bottomPos, $rightPos, $topPos);

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
            ->drawText('S.C. MLS S.R.L.', $pageWidth/2+70, $pageHeight-18)
            ->drawText('Cal. Dorobantilor, Bl. 9, Sc. B, Ap. 19', $pageWidth/2+70, $pageHeight-30)
            ->drawText('300298, Timisoara, Timis', $pageWidth/2+70, $pageHeight-42)
            ->drawText(getText('Reg. Com. ') . 'J35/1001/2004', $pageWidth/2+70, $pageHeight-54)
            ->drawText(getText('C.U.I.: ') . 'RO 16311704', $pageWidth/2+70, $pageHeight-66)
            ->drawText('Tel.: +40 256 212 489 / Fax: +40 372 895 991', $pageWidth/2+70, $pageHeight-78)
            ->drawText(getText('Banca: ') . 'UNICREDIT TIRIAC BANK', $pageWidth/2+70, $pageHeight-90)
            ->drawText('IBAN: RO61BACX0000000595790000', $pageWidth/2+70, $pageHeight-102);

        $page2->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
            ->drawText('S.C. MLS S.R.L.', $pageWidth/2+70, $pageHeight-18)
            ->drawText('Cal. Dorobantilor, Bl. 9, Sc. B, Ap. 19', $pageWidth/2+70, $pageHeight-30)
            ->drawText('300298, Timisoara, Timis', $pageWidth/2+70, $pageHeight-42)
            ->drawText(getText('Reg. Com. ') . 'J35/1001/2004', $pageWidth/2+70, $pageHeight-54)
            ->drawText(getText('C.U.I.: ') . 'RO 16311704', $pageWidth/2+70, $pageHeight-66)
            ->drawText('Tel.: +40 256 212 489 / Fax: +40 372 895 991', $pageWidth/2+70, $pageHeight-78)
            ->drawText(getText('Banca: ') . 'UNICREDIT TIRIAC BANK', $pageWidth/2+70, $pageHeight-90)
            ->drawText('IBAN: RO61BACX0000000595790000', $pageWidth/2+70, $pageHeight-102);

        $page->setStyle($boldLine)
            ->drawRectangle(15, 723, $pageWidth-15, $pageHeight-117);
        $page->setStyle($blackStyle);

        $page2->setStyle($boldLine)
            ->drawRectangle(15, 723, $pageWidth-15, $pageHeight-117);
        $page2->setStyle($blackStyle);

        // Numar Rezervare
        $textTop = $pageHeight-145;

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
            ->drawText(getText('Comanda de transfer/Contract Nr.') . ' TM' . $orderEntity->getField('uniqueId') . '/' . date("d.m.Y", $orderEntity->getField('createdAt')->sec), $pageLeftSpace, $textTop);

        // Inforamti ruta
        $textTop -= 40;

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 13)
            ->drawText(getText('Informatii despre ruta'), $pageLeftSpace, $textTop);


        // DE LA
        $textTop -= 30;

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 11)
            ->drawText(getText('De la: '), $pageLeftSpace + 10, $textTop);

//        if ($durationFlag) {
//            $returnSchedule = 1;
//            if (false === is_null($bookingEntity->getField('returnSchedule'))) {
//                $returnSchedule = 2;
//            }
//
//            // Distance
//            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 11)
//                ->drawText(getText('Distanta: '), $pageWidth/2+20, $textTop-20);
//
//            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
//                ->drawText(round(($bookingEntity->getField('effort')->getField('distance') / 1000) * $returnSchedule, 1) . " km", $pageWidth/2+75, $textTop-20);
//
//            // Duration
//            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 11)
//                ->drawText(getText('Durata: '), $pageWidth/2+20, $textTop);
//
//            $durationSeconds = $bookingEntity->getField('effort')->getField('duration') * $returnSchedule;
//            $durationMinutes = ceil(($durationSeconds % 3600) / 60) . "m";
//            $durationHours   = ($durationSeconds > 3600) ? floor($durationSeconds / 3600) . "h" : '';
//
//            $duration = $durationHours . " " . $durationMinutes;
//
//            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
//                ->drawText($duration, $pageWidth/2+65, $textTop);
//        }
        
        $lines = $this->wrapText(70, $origin);

        foreach ($lines as $lineText) {
            $textTop -= 15;

            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                ->drawText($lineText , $pageLeftSpace + 20, $textTop);
        }

        // PANA LA
        $textTop -= 25;

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 11)
            ->drawText($destinationType.': ', $pageLeftSpace + 10, $textTop);


        $lines = $this->wrapText(70, $destinationValue);

        foreach ($lines as $lineText) {
            $textTop -= 15;

            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                ->drawText($lineText , $pageLeftSpace + 20, $textTop);
        }

        $textTop -= 25;

        $waypointsArray = $bookingEntity->getField('waypoints');

        if (!is_null($waypointsArray) && !empty($waypointsArray)) {

            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 11)
                ->drawText(getText('Opriri: '), $pageLeftSpace + 10, $textTop);

            $textTop -= 5;
            foreach ($bookingEntity->getField('waypoints') as $key => $waypoint) {

                $lines = $this->wrapText(70, $this->convertText($waypoint));

                foreach ($lines as $lineNr => $lineText) {
                    $textTop -= 10;

                    if ($lineNr === 0) {
                        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                            ->drawText('-', $pageLeftSpace + 20, $textTop);
                    }

                    $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                        ->drawText($lineText , $pageLeftSpace + 25, $textTop);
                }
                $textTop -= 5;
            }

            $textTop -= 5;
        }

        
        if ($orderEntity->getField('references')->getField('booking')->getField('voucher')) {
            $voucher = $orderEntity->getField('references')->getField('booking')->getField('voucher');
            
            $textTop -= 25;
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 13)
                ->drawText(getText('Voucher'), $pageLeftSpace, $textTop);
            
            $textTop -= 20;
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                ->drawText(getText('Descriere: ') . $voucher->getField('name'), $pageLeftSpace, $textTop);

            $textTop -= 15;
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                ->drawText(getText('Cod: ') . $voucher->getField('code'), $pageLeftSpace, $textTop);

            $textTop -= 15;
            $discountMethod = ($voucher->getField('discountMethod') === 'percent') ? '%' : 'Euro'; 
            
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                ->drawText(getText('Discount: ') . $voucher->getField('discount').' '.$discountMethod, $pageLeftSpace, $textTop);
            
            $textTop -= 15;
        }

        $leftSpace = $textTop;
        $rightSpace = $textTop;

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 13)
            ->drawText(getText('Informatii despre calatorie'), $pageLeftSpace, $leftSpace-25);

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
            ->drawText($scheduleTypeName['data'].': '.date("j m Y", strtotime($bookingEntity->getField('schedule')->getField('date'))), $pageLeftSpace, $leftSpace-45)
            ->drawText($scheduleTypeName['ora'].': '.$bookingEntity->getField('schedule')->getField('hour').':'.$bookingEntity->getField('schedule')->getField('minute'), $pageLeftSpace, $leftSpace-60);

        if ($bookingEntity->getField('returnSchedule')) {
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                ->drawText($returnScheduleTypeName['data'].': '.date("j m Y", strtotime($bookingEntity->getField('returnSchedule')->getField('date'))), $pageLeftSpace, $leftSpace-75)
                ->drawText($returnScheduleTypeName['ora'].': '.$bookingEntity->getField('returnSchedule')->getField('hour').':'.$bookingEntity->getField('returnSchedule')->getField('minute'), $pageLeftSpace, $leftSpace-90);
            $leftSpace -= 30;
        }

        if ($bookingEntity->getField('flightNumber')) {
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                ->drawText(getText('Numar zbor: ').$bookingEntity->getField('flightNumber'), $pageLeftSpace, $leftSpace-75);
            $leftSpace -= 15;
        }

        if ($bookingEntity->getField('returnFlightNumber')) {
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                ->drawText(getText('Numar zbor retur: ').$bookingEntity->getField('returnFlightNumber'), $pageLeftSpace, $leftSpace-75);
            $leftSpace -= 15;
        }

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
            ->drawText(getText('Pasageri: ').$bookingEntity->getField('passengers')->getField('adults').getText(' adulti, ').$bookingEntity->getField('passengers')->getField('children').getText(' copii'), $pageLeftSpace, $leftSpace-75)
            ->drawText(getText('Masini: ').$bookingEntity->getField('selectedCarCombination')->getField('quantity').' x '.ucfirst($bookingEntity->getField('selectedCarCombination')->getField('type')), $pageLeftSpace, $leftSpace-90);

        if ($bookingEntity->getField('selectedCarCombination')->getField('priceWithDiscount')) {
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                ->drawText(getText('Pret: ') . number_format($bookingEntity->getField('selectedCarCombination')->getField('priceWithDiscount'), 2).' EUR', $pageLeftSpace, $leftSpace-105)
                ->drawText('Discount: '.$this->carApi->getDiscountAmount($bookingEntity->getField('schedule'), $bookingEntity->getField('selectedCarCombination')->getField('discount')).'%', $pageLeftSpace, $leftSpace-120);
            $leftSpace -= 15;

            if ($bookingEntity->getField('returnSchedule')) {
                $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                    ->drawText(getText('Discount retur: ') . $this->carApi->getDiscountAmount($bookingEntity->getField('returnSchedule'), $bookingEntity->getField('selectedCarCombination')->getField('discount')).'%', $pageLeftSpace, $leftSpace-120);
                $leftSpace -= 15;
            }

        } else {
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                ->drawText(getText('Pret: ').$bookingEntity->getField('selectedCarCombination')->getField('price').' EUR', $pageLeftSpace, $leftSpace-105);
        }

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 13)
            ->drawText(getText('Informatii despre contact'), $pageWidth/2+20, $rightSpace-25);
        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
            ->drawText($userDetails['name'], $pageWidth/2+20, $rightSpace-45)
            ->drawText($userDetails['email'], $pageWidth/2+20, $rightSpace-60)
            ->drawText($userDetails['phone'], $pageWidth/2+20, $rightSpace-75)
            ->drawText($userDetails['address'], $pageWidth/2+20, $rightSpace-90);

        if (!is_null($orderEntity->getField('companyInformation'))) {
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 13)
                ->drawText(getText('Informatii factura persoana juridica'), $pageLeftSpace, $leftSpace-145);
            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                ->drawText(getText('Companie: ') . $orderEntity->getField('companyInformation')->getField('name'), $pageLeftSpace, $leftSpace-165);

            $leftSpace -= 180;

            $companyAddress = $orderEntity->getField('companyInformation')->getField('address');
            if (!empty($companyAddress)) {
                $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                    ->drawText(getText('Adresa: ') . $orderEntity->getField('companyInformation')->getField('address'), $pageLeftSpace, $leftSpace);
                $leftSpace -= 15;
            }

            $companyPhone = $orderEntity->getField('companyInformation')->getField('phone');
            if (!empty($companyPhone)) {
                $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                    ->drawText(getText('Telefon: ') . $orderEntity->getField('companyInformation')->getField('phone'), $pageLeftSpace, $leftSpace);
                $leftSpace -= 15;
            }

            $companyFax = $orderEntity->getField('companyInformation')->getField('fax');
            if (!empty($companyFax)) {
                $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                    ->drawText('Fax: ' . $orderEntity->getField('companyInformation')->getField('fax'), $pageLeftSpace, $leftSpace);
                $leftSpace -= 15;
            }

            $companyCui = $orderEntity->getField('companyInformation')->getField('CUI');
            if (!empty($companyCui)) {
                $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                    ->drawText(getText('CUI/CIF: ') . $orderEntity->getField('companyInformation')->getField('CUI'), $pageLeftSpace, $leftSpace);
                $leftSpace -= 15;
            }

            $companyJ = $orderEntity->getField('companyInformation')->getField('J');
            if (!empty($companyJ)) {
                $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 11)
                    ->drawText(getText('Nr. Reg. Com.: ') . $orderEntity->getField('companyInformation')->getField('J'), $pageLeftSpace, $leftSpace);
            }
        }

        $observations = $this->convertText($orderEntity->getField('observations'));

        if (!empty($observations)) {

            $page3 = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);

            $page3->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 10)
                ->drawText(getText('Observatii comanda:'), $pageLeftSpace, $page3->getHeight()-160);

            $vertical = $page3->getHeight()-180;
            $lines    = $this->wrapText(105, $observations);

            foreach($lines as $key => $line) {

                $verticalAxe = $vertical - 15 * $key;
                $page3->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 10)
                    ->drawText($line, $pageLeftSpace, $verticalAxe);
            }

            // Display header, observations and footer on a new page
            $page3->drawImage($logo, $leftPos, $bottomPos, $rightPos, $topPos);
            $page3->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
                ->drawText('S.C. MLS S.R.L.', $pageWidth/2+70, $pageHeight-18)
                ->drawText('Cal. Dorobantilor, Bl. 9, Sc. B, Ap. 19', $pageWidth/2+70, $pageHeight-30)
                ->drawText('300298, Timisoara, Timis', $pageWidth/2+70, $pageHeight-42)
                ->drawText(getText('Nr. Reg. Com.: ') . 'J35/1001/2004', $pageWidth/2+70, $pageHeight-54)
                ->drawText(getText('C.U.I.: ') . 'RO 16311704', $pageWidth/2+70, $pageHeight-66)
                ->drawText('Tel.: +40 256 212 489 / Fax: +40 372 895 991', $pageWidth/2+70, $pageHeight-78)
                ->drawText(getText('Banca: ') . 'UNICREDIT TIRIAC BANK', $pageWidth/2+70, $pageHeight-90)
                ->drawText('IBAN: RO61BACX0000000595790000', $pageWidth/2+70, $pageHeight-102);

            $page3->setStyle($boldLine)
                ->drawRectangle(15, 723, $pageWidth-15, $pageHeight-117);
            $page3->setStyle($blackStyle);

            $page3->setStyle($boldLine)
                ->drawRectangle(15, 65, $pageWidth-15, $pageHeight-776);
            $page3->setStyle($blackStyle);

            $page3->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
                ->drawText('Email: office@citylimo.ro, office@citycab.ro', $pageLeftSpace, $pageHeight-797)
                ->drawText('Website: www.citylimo.ro, www.citycab.ro, www.citycar.ro', $pageLeftSpace, $pageHeight-812);
        }

        $page->setStyle($boldLine)
            ->drawRectangle(15, 65, $pageWidth-15, $pageHeight-776);
        $page->setStyle($blackStyle);
        $page2->setStyle($boldLine)
            ->drawRectangle(15, 65, $pageWidth-15, $pageHeight-776);
        $page2->setStyle($blackStyle);

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
            ->drawText('Email: office@citylimo.ro, office@citycab.ro', $pageLeftSpace, $pageHeight-797)
            ->drawText('Website: www.citylimo.ro www.citycab.ro www.citycar.ro', $pageLeftSpace, $pageHeight-812);
        $page2->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
            ->drawText('Email: office@citylimo.ro, office@citycab.ro', $pageLeftSpace, $pageHeight-797)
            ->drawText('Website: www.citylimo.ro, www.citycab.ro, www.citycar.ro', $pageLeftSpace, $pageHeight-812);

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
            ->drawText('1', $pageWidth-30, $pageHeight-812);

        if (!empty($observations)) {
            $page3->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
                ->drawText('2', $pageWidth-30, $pageHeight-812);
            $page2->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
                ->drawText('3', $pageWidth-30, $pageHeight-812);
        } else {
            $page2->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
                ->drawText('2', $pageWidth-30, $pageHeight-812);
        }

        if ($language == "en") {
            $terms[] = $this->convertText("Prices for chauffeur service are calculated thus: the trip will begin on the date on which the vehicle/driver leaves the Branch City Limousine Service and ends with the return in the same location or confirmed at the time of booking. Prices include VAT ". $vatTax ."%, driver costs and petrol costs. For the rental of several days, covered parking and other fees are not included. For one night between 22:00 to 06:00 the next day, an additional charge of 5.00 EUR/night every hour part thereof, or 5.00 EUR/transfer. Night surcharge is already included in the price quoted. The calculation of overtime and time of calling is done per hour or part thereof.");
            $terms[] = $this->convertText("Cancellation with two hour or less before your departure (time until the driver/vehicle leaves City Limousine Service Branch) or no show, the passenger will be charged a cancellation fee: 100% of the invoiced amount. Unable to locate the driver in place agreed by the passenger is to prevent any cancellation fee mentioned above for a no-show. The passenger is obligated to inform City Limo in accordance with the number of 48 hour service.");
            $terms[] = $this->convertText("A particular manufacturer of vehicles cannot be always guaranteed.");
            $terms[] = $this->convertText("Payment of rent or transfer shall pay in advance: the rental price and all additional fees. Payments terms: we accept the following credit cards: Master Card, Visa. Prepaid cards, giro cards (debit cards) will not be accepted.");
            $terms[] = $this->convertText("The passenger has a warranty equal to the invoice amount agreed upon, plus a security surcharge of at least 100 EUR or more: 10% in addition to the amount of the invoice. For any information do not hesitate to contact us.");
        } else {
            $terms[] = $this->convertText("Preturile pentru serviciul cu sofer sunt calculate astfel: calatoria va incepe la data la care vehiculul / soferul paraseste filiala City Limousine Service si se incheie cu intoarcerea in aceasi locatie sau locatia confirmata in momentul rezervarii. Preturile includ " . $vatTax ."% TVA, sofer si costurile de combustibil. Pentru inchirierile de mai multe zile, parcarile si alte taxe nu sunt incluse. Pentru transfer nocturn intre ora 22:00 si ora 06:00 a doua zi, se percepe o taxa suplimentara de noapte, de 10,00 EUR / fiecare ora sau parte a acesteia, sau 20,00 EUR / transfer. Suprataxa de noapte este deja inclusa in pretul cotat. Calculul orelor suplimentare si timpul de asteptare se face pe ora sau o parte a acesteia.");
            $terms[] = $this->convertText("Pentru anularea cu doua ore sau mai putin inainte de plecare (= de timp la care soferul / vehicul paraseste filiala City Limousine Service) sau no-show (neprezentare), pasagerul va plati o taxa de anulare care reprezinta 100% din valoarea facturii emise. In cazul imposibilitatii de a localiza soferul la locul convenit de pasager pentru a preveni orice taxa de anulare mentionate mai sus pentru neprezentare Pasagerul are obligatia de a informa City Limo la numarul de serviciu valabil 24 ore, 7/7.");
            $terms[] = $this->convertText("Un anumit producator de vehicule nu poate fi intotdeauna garantat.");
            $terms[] = $this->convertText("Plata serviciilor de inchiriere sau transfer (pretul de Inchiriere, precum si toate taxele suplimentare) se va efectua inainte de inceperea serviciului cu urmatoarele conditii de plata: Acceptam carti de credit: Mastercard, Visa, Maestro. Nu se accepta la plata cardurile preplatite, girocards.");
            $terms[] = $this->convertText("Va oferim garantia calitatii. Pasagerul City Limousine Services are o garantie egala cu valoarea facturii convenite + 10% in cazul in care, din culpa noastra, serviciile de transfer nu sunt conform cererii facute de client. Pentru orice informatii nu ezitati sa ne contactati.");
        }

        $vertical = $pageHeight-157;
        foreach ($terms as $paragraph) {

            $lines = $this->wrapText(95, $paragraph);

            foreach($lines as $key => $line) {

                $verticalAxe = $vertical - 15 * $key;
                $page2->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 12)
                    ->drawText($line, $pageLeftSpace, $verticalAxe);
            }
            $vertical = $verticalAxe - 30;
        }

        $page2->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 12)
            ->drawText(getText('Semnatura'), $pageWidth-160, $pageHeight-602)
            ->drawText(getText('Data'), 80, $pageHeight-602);

        $pdf->pages[] = $page;

        if (!empty($observations)) {
            $pdf->pages[] = $page3;
        }

        $pdf->pages[] = $page2;

        if ($downloadPdf) {
            header('Content-type: application/pdf; charset=utf-8');
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename="' . getText('Confirmare Rezervare #') . $orderEntity->getField('uniqueId') . '.pdf"');
            header('Content-Transfer-Encoding: binary');
            header('Connection: Keep-Alive');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
        } else {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . getText('Confirmare Rezervare #') . $orderEntity->getField('uniqueId') . '.pdf"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
        }

        return $pdf->render();

    }

    /**
     * @param array $companyInfoArray
     *
     * @return OrderCompanyInfoEntity
     */
    public function createUserOrderCompanyInfoEntity($companyInfoArray = array()) {

        $orderCompanyInfoEntity = new OrderCompanyInfoEntity();
        $orderCompanyInfoEntity->populateFromArray($companyInfoArray);

        return $orderCompanyInfoEntity;
    }

    /**
     * @param CompanyEntity $companyEntity
     *
     * @return OrderCompanyInfoEntity
     */
    public function createCompanyOrderCompanyInfoEntity($userCompanyEntity, $sessionArrayCompanyInfo) {

        $orderCompanyInfoEntity = new OrderCompanyInfoEntity();

        foreach ($sessionArrayCompanyInfo as $key => $companyInfoItem) {
            $orderCompanyInfoEntity->setField($key, $sessionArrayCompanyInfo[$key]);
        }

        $departmentInfo = new OrderCompanyDepartmentInfoEntity();
        $departmentInfo->setField('name', $userCompanyEntity->getField('departmentName'));
        $departmentInfo->setField('id', $userCompanyEntity->getField('departmentId'));
        $orderCompanyInfoEntity->setField('departmentInformation', $departmentInfo);

        return $orderCompanyInfoEntity;
    }

    private function wrapText($numberOfCharacters, $text) {

        $wrappedText = wordwrap($text, $numberOfCharacters,"\n",TRUE);

        $lines = explode("\n", $wrappedText);

        return $lines;
    }

    private function convertText($string) {

        $pattern = array("'ș'", "'Ș'", "'Ţ'", "'ţ'", "'ț'", "'Ț'", "'Ş'", "'ş'", "'ă'", "'Ă'", "'é'", "'è'", "'ë'", "'ê'", "'É'", "'È'", "'Ë'", "'Ê'", "'á'", "'à'", "'ä'", "'â'", "'å'", "'Á'", "'À'", "'Ä'", "'Â'", "'Å'", "'ó'", "'ò'", "'ö'", "'ô'", "'Ó'", "'Ò'", "'Ö'", "'Ô'", "'í'", "'ì'", "'ï'", "'î'", "'Í'", "'Ì'", "'Ï'", "'Î'", "'ú'", "'ù'", "'ü'", "'û'", "'Ú'", "'Ù'", "'Ü'", "'Û'", "'ý'", "'ÿ'", "'Ý'", "'ø'", "'Ø'", "'œ'", "'Œ'", "'Æ'", "'ç'", "'Ç'");
        $replace = array("s", "S", "T", "t", "t", "T", "S", "s", "a", "A", 'e', 'e', 'e', 'e', 'E', 'E', 'E', 'E', 'a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A', 'A', 'o', 'o', 'o', 'o', 'O', 'O', 'O', 'O', 'i', 'i', 'i', 'I', 'I', 'I', 'I', 'I', 'u', 'u', 'u', 'u', 'U', 'U', 'U', 'U', 'y', 'y', 'Y', 'o', 'O', 'a', 'A', 'A', 'c', 'C');

        $string = preg_replace($pattern, $replace, $string);

        return $string;
    }

}
?>