<?php

namespace CL\Config\Mapper;

use \Xeeo\Services\Database\Mongo\Mapper as Mapper;

class Config extends Mapper {

    protected static $collection = 'config';
    protected static $entity     = '\CL\Config\Entity\Config';
}
?>