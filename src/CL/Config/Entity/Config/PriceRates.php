<?php

namespace CL\Config\Entity\Config;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class PriceRates extends AbstractEntity {

    const FIELD_AIRPORT_TAX  = 'airportTax';
    const FIELD_WAYPOINT_TAX = 'waypointTax';
    const FIELD_START_TAX    = 'startTax';
    const FIELD_VAT_TAX      = 'vatTax';
    const FIELD_CREATED      = 'created';
    const FIELD_UPDATED      = 'updated';

    public function initFields()
    {
        return array(
            self::FIELD_AIRPORT_TAX  => $this->field()
                    ->setRequired(false),
            self::FIELD_WAYPOINT_TAX => $this->field()
                    ->setRequired(false),
            self::FIELD_START_TAX    => $this->field()
                    ->setRequired(false),
            self::FIELD_VAT_TAX      => $this->field()
                    ->setRequired(false),
            self::FIELD_CREATED      => $this->field()
                    ->setRequired(false),
            self::FIELD_UPDATED      => $this->field()
                    ->setRequired(false)
        );
    }

    public function getAirportTax() {
        return $this->getField(self::FIELD_AIRPORT_TAX);
    }

    public function setAirportTax($airportTax) {
        $this->setField(self::FIELD_AIRPORT_TAX, $airportTax);
    }

    public function getWaypointTax() {
        return $this->getField(self::FIELD_WAYPOINT_TAX);
    }

    public function setWaypointTax($waypointTax) {
        $this->setField(self::FIELD_WAYPOINT_TAX, $waypointTax);
    }

    public function getStartTax() {
        return $this->getField(self::FIELD_START_TAX);
    }

    public function setStartTax($startTax) {
        $this->setField(self::FIELD_START_TAX, $startTax);
    }

    public function getVatTax() {
        return $this->getField(self::FIELD_VAT_TAX);
    }

    public function setVatTax($vatTax) {
        $this->setField(self::FIELD_VAT_TAX, $vatTax);
    }

    public function getCreated() {
        return $this->getField(self::FIELD_CREATED);
    }

    public function setCreated($created) {
        return $this->setField(self::FIELD_CREATED, $created);
    }

    public function getUpdated() {
        return $this->getField(self::FIELD_UPDATED);
    }

    public function setUpdated($update) {
        $this->setField(self::FIELD_UPDATED, $update);
    }
}