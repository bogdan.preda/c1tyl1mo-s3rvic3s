<?php

namespace CL\Config\Entity;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity,
    \CL\Config\Entity\Config\PriceRates as PriceRatesEntity;

class Config extends AbstractEntity {

    const FIELD_ID          = '_id';
    const FIELD_PRICE_RATES = 'priceRates';
    
    public function initFields() {
        return array(
            "__v"                => $this->field()
                ->setIgnore(true),
            self::FIELD_ID => $this->field()
                ->setIgnore(true),
            self::FIELD_PRICE_RATES => $this->field()
                ->setType('CL\Config\Entity\Config\PriceRates')
                ->setIgnore(true)
        );
    }

    public function getId() {
        return $this->getField(self::FIELD_ID);
    }
    
    /**
     * @return PriceRatesEntity
     */
    public function getPriceRates() {
        return $this->getField(self::FIELD_PRICE_RATES);
    }

    public function setPriceRates(PriceRatesEntity $priceRates) {
        $this->setField(self::FIELD_PRICE_RATES, $priceRates);
    }
} 
?>