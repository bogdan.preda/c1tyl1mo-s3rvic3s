<?php

namespace CL\Config;

use \CL\Config\Entity\Config as ConfigEntity,
    \CL\Config\Mapper\Config as ConfigMapper,
    \CL\Core\ApiAbstract;


class Api extends ApiAbstract {

    public function init() {

        parent::init();
    }

    /**
     * we only have one config entity, instead fo keeping some configuration params in files, we moved them into DB
     * 
     * @return ConfigEntity
     */
    public function getConfigEntity() {

        $result   = ConfigMapper::findOne();

        return $result;
    }

    /**
     * @param $configEntity ConfigEntity
     */
    public function saveEntity($configEntity) {
        return ConfigMapper::save($configEntity);
    }
}


?>


