<?php

namespace CL\Booking\Entity;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity,
    \CL\Voucher\Mapper\Voucher as VoucherMapper;

class Booking extends AbstractEntity {

    public function initFields() {

        return array(
            "__v"                => $this->field()
                    ->setIgnore(true),
            '_id'                    => $this->field()
                    ->setIgnore(true),
            'routeType'              => $this->field()
                    ->setRequired(true),
            'origin'                 => $this->field()
                    ->setRequired(true),
            'destination'            => $this->field()
                    ->setIgnore(true),
            'waypoints'              => $this->field()
                    ->setIgnore(true),
            'roundTrip'              => $this->field()
                    ->setValue(false),
            'applyAirportTax' => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false),
            'schedule'               => $this->field()
                    ->setRequired(false)
                    ->setType('CL\Booking\Entity\Booking\Schedule'),
            'returnSchedule'         => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false)
                    ->setType('CL\Booking\Entity\Booking\Schedule'),
            'flightNumber'       => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false),
            'returnFlightNumber' => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false),
            'selectedCarCombination' => $this->field()
                    ->setRequired(false)
                    ->setType('CL\Booking\Entity\Booking\SelectedCarCombination'),
            'effort'                 => $this->field()
                    ->setRequired(true)
                    ->setType('CL\Booking\Entity\Booking\Effort'),
            'passengers'             => $this->field()
                    ->setRequired(true)
                    ->setType('CL\Booking\Entity\Booking\Passengers'),
            'voucher'             => $this->field()
                    ->setReference(new VoucherMapper())
                    ->setIgnore(true)
                    ->setRequired(false),
            'baggages'               => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false),
            "createdAt"              => $this->field()
                    ->setRequired(false)
        );
    }
}