<?php

namespace CL\Booking\Entity\Booking;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class Effort extends AbstractEntity {

    public function initFields()
    {
        return array(
            "distance" => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false),
            "duration" => $this->field()
                    ->setRequired(true)
        );
    }
}
?>
