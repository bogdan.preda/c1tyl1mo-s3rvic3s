<?php

namespace CL\Booking\Entity\Booking;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class Schedule extends AbstractEntity {

    public function initFields()
    {
        return array(
            "date"   => $this->field()
                    ->setRequired(true),
            "hour"   => $this->field()
                    ->setRequired(true),
            "minute" => $this->field()
                    ->setRequired(true),
            "type"   => $this->field()
                    ->setRequired(true),
            "timestamp" => $this->field()
                    ->setIgnore(true)
        );
    }

}
?>
