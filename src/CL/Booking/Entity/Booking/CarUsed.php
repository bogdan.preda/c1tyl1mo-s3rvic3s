<?php

namespace CL\Booking\Entity\Booking;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class CarUsed extends AbstractEntity {

    public function initFields()
    {
        return array(
            "carType"  => $this->field()
                    ->setRequired(true),
            "quantity" => $this->field()
                    ->setRequired(true),
        );
    }

}
?>
