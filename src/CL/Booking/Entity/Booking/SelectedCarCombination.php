<?php

namespace CL\Booking\Entity\Booking;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class SelectedCarCombination extends AbstractEntity {

    public function initFields()
    {
        return array(
            "quantity" => $this->field()
                ->setRequired(true),
            "type"     => $this->field()
                ->setRequired(true),
            /**
             * price as in totalPrice
             */
            "price"    => $this->field()
                ->setRequired(true),
            "priceWithDiscount" => $this->field()
                ->setRequired(false)->setIgnore(true),
            "discount"          => $this->field()
                ->setType('CL\Company\Entity\Company\Discount')
                ->setRequired(false)->setIgnore(true),
            "seatsRemaining" => $this->field()
                ->setRequired(true),
            "text"           => $this->field()
                ->setRequired(false)
        );
    }
}
?>
