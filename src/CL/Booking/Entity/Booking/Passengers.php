<?php

namespace CL\Booking\Entity\Booking;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class Passengers extends AbstractEntity {

    public function initFields()
    {
        return array(
            "adults" => $this->field()
                    ->setRequired(true),
            "children" => $this->field()
                    ->setRequired(true),
            "total" => $this->field()
                    ->setRequired(true)
        );
    }

}
?>
