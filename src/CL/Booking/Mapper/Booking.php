<?php

namespace CL\Booking\Mapper;

use \Xeeo\Services\Database\Mongo\Mapper as Mapper;

class Booking extends Mapper {

    protected static $collection = 'bookings';
    protected static $entity     = '\CL\Booking\Entity\Booking';

}
?>