<?php

namespace CL\Booking;

use \CL\Core\ApiAbstract,
    \CL\Booking\Entity\Booking as BookingEntity,
    \CL\Booking\Mapper\Booking as BookingMapper,
    \Xeeo\Services\Database\Mongo\Filter;

class Api extends ApiAbstract {

    public function init() {

        parent::init();
    }

    /**
     * Returns collection of orders by a given filter
     *
     * @param array $criteria
     * @param array $sort
     * @param int   $page
     * @param int   $itemsPerPage
     *
     * @return \ArrayObject
     */
    public function findBookingsBy($criteria, $sort = array(), $page = 1, $itemsPerPage = self::ITEMS_PER_PAGE)
    {
        $itemsPerPage = (empty($itemsPerPage)) ? $itemsPerPage : abs((int) $itemsPerPage);
        $page         = (empty($page)) ? 1 : abs((int) $page);
        $offset       = ($page - 1) * $itemsPerPage;

        if (false === is_array($sort)) {
            $sort = array();
        }

        $result = BookingMapper::find($criteria, $itemsPerPage, $offset, $sort);

        return $result;
    }

    public function getBooking($id)
    {
        $result = BookingMapper::get($id);

        return $result;
    }

    /**
     * Returns timestamp
     *
     * @return int
     */
    public function getLatestBookingSchedule()
    {
        /**
         * TODO change this into exits => true
         */
        $timestampExistsCriteria = Filter::set('schedule.timestamp', Filter::NOT_EQUAL, null);
        $sort                    = array('schedule.timestamp' => -1);
        $latestBookingSchedule   = time();

        $lastBooking = $this->findBookingsBy($timestampExistsCriteria, $sort, 1, 1);

        if (count($lastBooking->getEntities()) == 1) {
            $latestBookingSchedule = $lastBooking->getEntities()[0]->getField('schedule')->getField('timestamp');
        }

        return $latestBookingSchedule;
    }
}
?>