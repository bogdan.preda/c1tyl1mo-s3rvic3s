<?php

namespace CL\Company\Entity;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity,
    \CL\Company\Entity\Company as CompanyEntity,
    \CL\Company\Entity\Company\Discount as DiscountEntity,
    \CL\Company\Entity\Company\Departments as DepartmentsEntity,
    \CL\Company\Entity\Company\ServicesDiscount as ServicesDiscountEntity,
    \CL\User\Mapper\User as UserMapper;

class Company extends AbstractEntity {

    public function initFields() {
        return array(
            "__v"                => $this->field()
                    ->setIgnore(true),
            "_id"                    => $this->field()
                    ->setIgnore(true),
            "name"                   => $this->field()
                    ->setRequired(false),
            "owner"                  => $this->field()
                    ->setReference(new UserMapper())
                    ->setRequired(false),
            "departmentsInformation" => $this->field()
                    ->setRequired(false)
                    ->setType('CL\Company\Entity\Company\Departments'),
            "servicesDiscount"       => $this->field()
                    ->setType('CL\Company\Entity\Company\ServicesDiscount'),
            "companyInformation"     => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false)
                    ->setType('CL\Company\Entity\Company\CompanyInformation'),
            "createdAt"              => $this->field()
                    ->setRequired(true)
        );
    }

    /**
     * @return ServicesDiscountEntity
     */
    public function getServicesDiscount() {
        return $this->getField('servicesDiscount');
    }

    public function setServicesDiscount($servicesDiscount) {
        $this->setField('servicesDiscount', $servicesDiscount);
    }
    
    /**
     * @return DepartmentsEntity
     */
    public function getDepartmentsInfo() {
        return $this->getField('departmentsInformation');
    }
    
    public function setDepartmentsInfo($departmentsInfoEntity) {
        $this->setField('departmentsInformation', $departmentsInfoEntity);
    }
}
?>