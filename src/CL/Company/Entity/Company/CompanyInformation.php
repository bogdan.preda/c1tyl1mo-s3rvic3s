<?php

namespace CL\Company\Entity\Company;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class CompanyInformation extends AbstractEntity {

    public function initFields() {

        return array(
            'address' => $this->field()->setRequired(false),
            'phone'   => $this->field()->setRequired(false),
            'fax'     => $this->field()->setRequired(false),
            'CUI'     => $this->field()->setRequired(false),
            'J'       => $this->field()->setRequired(false)
        );
    }
}