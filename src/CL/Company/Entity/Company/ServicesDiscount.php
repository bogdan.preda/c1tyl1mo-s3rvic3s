<?php

namespace CL\Company\Entity\Company;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity,
    \CL\Car\Entity\Car as CarEntity,
    \CL\Company\Entity\Company\Discount as DiscountEntity;

/**
 * Class ServicesDiscount
 *
 * @package CL\Company\Entity\Company
 */
class ServicesDiscount extends AbstractEntity {
    
    const ROUTE_DIRECT_TRANSFER = CarEntity::ROUTE_DIRECT_TRANSFER;
    const ROUTE_MULTIPLE_STOPS  = CarEntity::ROUTE_MULTIPLE_STOPS;
    const ROUTE_RENT_PER_HOUR   = CarEntity::ROUTE_RENT_PER_HOUR;
    const ROUTE_FROM_AIRPORT    = CarEntity::ROUTE_FROM_AIRPORT;
    const ROUTE_TO_AIRPORT      = CarEntity::ROUTE_TO_AIRPORT;
    
    public function initFields()
    {
        return array(
            self::ROUTE_DIRECT_TRANSFER => $this->field()
                ->setType('CL\Company\Entity\Company\Discount'),
            self::ROUTE_MULTIPLE_STOPS  => $this->field()
                ->setType('CL\Company\Entity\Company\Discount'),
            self::ROUTE_RENT_PER_HOUR   => $this->field()
                ->setType('CL\Company\Entity\Company\Discount'),
            self::ROUTE_FROM_AIRPORT    => $this->field()
                ->setType('CL\Company\Entity\Company\Discount'),
            self::ROUTE_TO_AIRPORT      => $this->field()
                ->setType('CL\Company\Entity\Company\Discount')
        );
    }
    
    public function setDiscountFor($field, DiscountEntity $discount)
    {
        $this->setField($field, $discount);
    }
}