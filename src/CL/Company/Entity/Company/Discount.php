<?php

namespace CL\Company\Entity\Company;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class Discount extends AbstractEntity {

    public function initFields() {

        return array(
            'businessHours'      => $this->field()->setRequired(false)->setValue(0),
            'extraBusinessHours' => $this->field()->setRequired(false)->setValue(0)
        );
    }
    
    public function getBusinessHours() {
        return $this->getField('businessHours');
    }
    
    public function setBusinessHours($businessHours) {
        $this->setField('businessHours', $businessHours);
    }
    
    public function getExtraBusinessHours() {
        return $this->getField('extraBusinessHours');
    }

    public function setExtraBusinessHours($extraBusinessHours) {
        $this->setField('extraBusinessHours', $extraBusinessHours);
    }
}