<?php

namespace CL\Company\Entity\Company;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity,
    \CL\Company\Entity\Company\Department as DepartmentEntity;

class Departments extends AbstractEntity {

    const FIELD_DEPT_COUNTER          = 'everCreated';
    const FIELD_DEFAULT_DEPARTMENT_ID = 'defaultDepartmentId'; 
    const FIELD_DEPARTMENTS           = 'departments';

    public function initFields() {

        return array(
            self::FIELD_DEPT_COUNTER => $this->field()
                    ->setRequired(true)
                    ->setValue(0),
            self::FIELD_DEFAULT_DEPARTMENT_ID => $this->field()
                    ->setRequired(false),
            self::FIELD_DEPARTMENTS  => $this->field()
                    ->setRequired(false)
                    ->setType('collection'),
        );
    }

    /**
     * Sets Departments Entity
     *
     */
    public function setDepartments($departments) {

        $this->setField(self::FIELD_DEPARTMENTS, $departments);
    }

    /**
     * @return Collection
     */
    public function getDepartments() {

        return $this->getField(self::FIELD_DEPARTMENTS);
    }

    /**
     * adds a department entity
     *
     * @param DepartmentEntity $department
     */
    public function addDepartment($department) {

        $departments     = $this->getDepartments();
        $departmentIndex = $this->getDepartmentIndexById($department->getField(DepartmentEntity::FIELD_ID));
        if(true === $this->updateDepartmentsCounter($departmentIndex)) {
            /**
             * generate ID for every new department
             */
            $department->generateId($this->getField(self::FIELD_DEPT_COUNTER));
        }
        $departments[$departmentIndex] = $department;

        $this->setDepartments($departments);
    }

    /**
     * @param $departmentName
     *
     * @return DepartmentEntity
     */
    public function getDepartmentById($departmentId) {

        $departmentsEntity = $this->getDepartments();

        if ($departmentsEntity->count() == 0) {
            return new DepartmentEntity();
        }

        foreach ($departmentsEntity as $index => $department) {

            if ($departmentId == $department->getField(DepartmentEntity::FIELD_ID)) {
                return $department;
            }
        }
        /**
         * In case no department was found
         */
        return new DepartmentEntity();
    }

    public function getDepartmentIndexById($departmentId) {

        $departmentsEntity = $this->getDepartments();

        foreach ($departmentsEntity as $index => $department) {

            if ($departmentId == $department->getField(DepartmentEntity::FIELD_ID)) {
                return $index;
            }
        }
        return $departmentsEntity->count();
    }

    /**
     * updates the department counter;
     * returns true in case of new department OR false in case of existing department
     * 
     * @param int $departmentIndex
     *
     * @return bool
     */
    public function updateDepartmentsCounter($departmentIndex = 0) {

        if ($departmentIndex >= $this->getDepartments()->count()) {
            $initialValue = $this->getField(self::FIELD_DEPT_COUNTER);
            $this->setField(self::FIELD_DEPT_COUNTER, $initialValue + 1);
            
            return true;
        }
        return false;
    }
}