<?php

namespace CL\Company\Entity\Company;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class Department extends AbstractEntity {

    const FIELD_ID        = 'id';
    const FIELD_NAME      = 'name';
    const FIELD_USERS     = 'users';
    const FIELD_STATUS    = 'status';
    const FIELD_READ_ONLY = 'readOnly';
    const FIELD_OBS       = 'observations';

    const STATUS_ACTIVE  = 'active';
    const STATUS_DELETED = 'deleted';
    
    const DEFAULT_DEPARTMENT_NAME = 'No Department';

    public function initFields() {

        return array(
            self::FIELD_ID        => $this->field()->setRequired(false),
            self::FIELD_NAME      => $this->field()->setRequired(false),
            self::FIELD_USERS     => $this->field()->setRequired(false)->setIgnore(true),
            self::FIELD_STATUS    => $this->field()->setRequired(true)->setValue(self::STATUS_ACTIVE),
            self::FIELD_READ_ONLY => $this->field()->setRequired(true)->setValue(false),
            self::FIELD_OBS       => $this->field()->setRequired(false)->setIgnore(true)
        );
    }

    public function setId($id) {
        $this->setField(self::FIELD_ID, $id);
    }
    
    public function getUsers() {
        return $this->getField(self::FIELD_USERS);
    }

    public function setUsers($users) {
        $this->setField(self::FIELD_USERS, $users);
    }

    public function addUser($userReference) {
        $users   = $this->getUsers();
        $users[] = $userReference;

        $this->setUsers($users);
    }

    public function removeUser($userId) {
        $users = $this->getUsers();

        foreach ($users as $index => $user) {
            if ($user['$id']->__toString() == $userId) {
                unset($users[$index]);
                break;
            }
        }
        $users   = array_values($users);
        $this->setUsers($users);
    }
    
    public function generateId($counter) {
        $range  = range('a', 'z');
        $letter = $range[rand(0, 25)];
        
        $id = $letter . date("ymd") . $counter;
        $this->setId($id);
    }

    public function getStatus() {
        return $this->getField(self::FIELD_STATUS);
    }

    /**
     * sets department status;
     * if status is changed into DELETED, the department is not shown in the listing anymore;
     *
     * @param $status
     */
    public function setStatus($status) {
        $this->setField(self::FIELD_STATUS, $status);
    }
}