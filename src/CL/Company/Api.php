<?php

namespace CL\Company;

use \CL\Company\Entity\Company,
    \CL\Core\ApiAbstract,
    \CL\Company\Entity\Company as CompanyEntity,
    \CL\Company\Entity\Company\Discount as DiscountEntity,
    \CL\Company\Entity\Company\ServicesDiscount as ServicesDiscountEntity,
    \CL\Company\Entity\Company\Department as DepartmentEntity,
    \CL\Company\Entity\Company\Departments as DepartmentsEntity,
    \CL\Company\Entity\Company\CompanyInformation as CompanyInfoEntity,
    \CL\User\Mapper\User as UserMapper,
    \CL\Company\Mapper\Company as CompanyMapper,
    \CL\Order\Mapper\Order as OrderMapper,
    \CL\Order\Api as OrderApi,
    \Xeeo\Services\Database\Mongo\Filter,
    \Xeeo\Services\Database\Mongo\Group,
    \Xeeo\Services\Core\Abstracts\Collection;


class Api extends ApiAbstract {

    const ITEMS_PER_PAGE  = -1;
    const ORDERS_PER_PAGE = 35;

    /**
     * @var OrderApi
     */
    private $orderApi;

    private $pageWidth;
    private $pageHeight;
    private $logo;
    private $logoImageHeight;
    private $logoImageWidth;
    private $pageMarginLeft;
    private $pageNumber = 1;

    public function init() {

        parent::init();

        $this->orderApi = OrderApi::getInstance();
    }

    public function createCompanyEntityFromRegister($registerEntity, $ownerEntity) {

        $companyEntity     = new CompanyEntity();
        $departmentsEntity = new DepartmentsEntity();
        $discountEntity    = new DiscountEntity();
        $companyInfoEntity = new CompanyInfoEntity();
        $servicesDiscountEntity = new ServicesDiscountEntity();
        $defaultDepartment = $this->createDefaultDepartment($ownerEntity);

        $departmentsEntity->setDepartments(new Collection());
        $departmentsEntity->addDepartment($defaultDepartment);
        $departmentsEntity->setField($departmentsEntity::FIELD_DEFAULT_DEPARTMENT_ID, $defaultDepartment->getField('id'));

        $discountEntity->setField('businessHours', floatval(0.00));
        $discountEntity->setField('extraBusinessHours', floatval(0.00));

        $servicesDiscountEntity->setDiscountFor($servicesDiscountEntity::ROUTE_DIRECT_TRANSFER, $discountEntity);
        $servicesDiscountEntity->setDiscountFor($servicesDiscountEntity::ROUTE_MULTIPLE_STOPS, $discountEntity);
        $servicesDiscountEntity->setDiscountFor($servicesDiscountEntity::ROUTE_RENT_PER_HOUR, $discountEntity);
        $servicesDiscountEntity->setDiscountFor($servicesDiscountEntity::ROUTE_FROM_AIRPORT, $discountEntity);
        $servicesDiscountEntity->setDiscountFor($servicesDiscountEntity::ROUTE_TO_AIRPORT, $discountEntity);

        $companyInfoEntity->setField('CUI', $registerEntity->getField('companyCui'));

        $companyEntity->setField('name', $registerEntity->getField('companyName'));
        $companyEntity->setField('owner', $ownerEntity);
        $companyEntity->setField('departmentsInformation', $departmentsEntity);
        $companyEntity->setField('companyInformation', $companyInfoEntity);
        $companyEntity->setServicesDiscount($servicesDiscountEntity);
        $companyEntity->setField('createdAt', new \MongoDate(time()));

        return $companyEntity;
    }

    /**
     * Returns collection of companies by a given filter
     *
     * @param array $criteria
     * @param array $sort
     * @param int   $page
     * @param int   $itemsPerPage
     *
     * @return \ArrayObject
     */
    public function findCompaniesBy($criteria, $sort = array(), $page = 1, $itemsPerPage = null)
    {
        $itemsPerPage = (empty($itemsPerPage)) ? self::ITEMS_PER_PAGE : $itemsPerPage;
        $page         = (empty($page)) ? 1 : abs((int) $page);
        $offset       = ($itemsPerPage > 0) ? ($page - 1) * $itemsPerPage : 0;

        if (false === is_array($sort)) {
            $sort = array();
        }

        $result = CompanyMapper::find($criteria, $itemsPerPage, $offset, $sort);

        return $result;
    }

    /**
     * Get the company where company's id equals the given company id
     *
     * @param \MongoId $companyId
     *
     * @return CompanyEntity $result
     */
    public function getCompanyEntityByCompanyId($companyId) {

        $criteria = Filter::set('_id', Filter::EQUAL, new \MongoId($companyId));
        $result   = CompanyMapper::findOne($criteria);

        return $result;
    }

    /**
     * Get the company where user's id equals the given user id
     *
     * @param \MongoId $userId
     *
     * @return CompanyEntity $result
     */
    public function getCompanyEntityByUserId($userId) {

        $criteria = Filter::set('owner.$id', Filter::EQUAL, new \MongoId($userId));
        $result   = CompanyMapper::findOne($criteria);

        return $result;
    }

    public function getCompanyEntityByEmployeeId($userId) {

        $criteria = Filter::set('departmentsInformation.departments.entities.users.$id', Filter::EQUAL, new \MongoId($userId));
        $result   = CompanyMapper::findOne($criteria);

        return $result;
    }

    /**
     * Sets a discount in DB
     *
     * @param array $discount
     */
    public function setCompanyDiscount($discount) {

        $companyEntity = $this->getCompanyEntityByUserId($discount['companyOwner']);

        $DiscountEntity = new DiscountEntity();
        $DiscountEntity->setField('businessHours', floatval($discount['businessHours']));
        $DiscountEntity->setField('extraBusinessHours', floatval($discount['extraBusinessHours']));

        $companyEntity->getServicesDiscount()->setDiscountFor($discount['service'], $DiscountEntity);

        $result = CompanyMapper::save($companyEntity);

        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * Get company's orders
     *
     * @param \MongoId $companyId
     *
     * @return Collection $result
     */
    public function getCompanyOrders($companyId) {

        $criteria = Filter::set('references.company.$id', Filter::EQUAL, new \MongoId($companyId));
        $result   = OrderMapper::find($criteria, 0);

        return $result;
    }

    /**
     * Get company's orders by filters
     *
     * @param \MongoId $companyId
     * @param int $keyword
     * @param string $department
     * @param int $dateStart
     * @param int $dateEnd
     *
     * @return Collection $filteredOrders, int $ordersAmount
     */
    public function getCompanyOrdersByFilters($companyId, $keyword, $department, $dateStart, $dateEnd, $status) {

        $criteria = Filter::set('references.company.$id', Filter::EQUAL, new \MongoId($companyId));

        if (false === empty($keyword)) {
            $criteriaOrderId = Filter::set('uniqueId', Filter::EQUAL, intval($keyword));
            $criteria        = Group::set($criteria, Group::OPERATOR_AND, $criteriaOrderId);
        }

        if (false === empty($status)) {
            $criteriaStatus  = Filter::set('status', Filter::EQUAL, $status);
            $criteria        = Group::set($criteria, Group::OPERATOR_AND, $criteriaStatus);
        }

        $sort      = array("createdAt" => -1);

        $orders = OrderMapper::find($criteria, 0, 0, $sort);

        $ordersAmount = 0;

        $filteredOrders = new Collection();
        foreach ($orders as $orderDetails) {

            $userEntity    = $orderDetails->getField('references')->getField('user');
            $bookingEntity = $orderDetails->getField('references')->getField('booking');
            $bookingDate   = $this->orderApi->convertDateToTimestamp($bookingEntity->getField('schedule'));

            if (false === empty($department)) {
                if ( ($bookingDate >= $dateStart) && ($bookingDate <= $dateEnd) && ($userEntity->getField('company')->getField('departmentId') == $department) ) {
                    $filteredOrders[] = $orderDetails;

                    if ($bookingEntity->getField('selectedCarCombination')->getField('priceWithDiscount')) {
                        $ordersAmount += $bookingEntity->getField('selectedCarCombination')->getField('priceWithDiscount');
                    } else {
                        $ordersAmount += $bookingEntity->getField('selectedCarCombination')->getField('price');
                    }
                }
            } else {
                if ( ($bookingDate >= $dateStart) && ($bookingDate <= $dateEnd)) {
                    $filteredOrders[] = $orderDetails;

                    if ($bookingEntity->getField('selectedCarCombination')->getField('priceWithDiscount')) {
                        $ordersAmount += $bookingEntity->getField('selectedCarCombination')->getField('priceWithDiscount');
                    } else {
                        $ordersAmount += $bookingEntity->getField('selectedCarCombination')->getField('price');
                    }
                }
            }

        }

        return array('filteredOrders' => $filteredOrders, 'ordersAmount' => $ordersAmount);
    }

    /**
     * Get company's orders number
     *
     * @param \MongoId $companyId
     * @param int $dateStart
     * @param int $dateEnd
     *
     * @return int $totalNumber
     */
    public function getCompanyOrdersNumberByDate($companyId, $dateStart, $dateEnd) {

        $criteria = Filter::set('references.company.$id', Filter::EQUAL, new \MongoId($companyId));
        $orders   = OrderMapper::find($criteria, 0);

        $totalNumber = 0;
        foreach ($orders as $orderDetails) {

            $bookingEntity = $orderDetails->getField('references')->getField('booking');
            $bookingDate   = $this->orderApi->convertDateToTimestamp($bookingEntity->getField('schedule'));

            if ( ($bookingDate >= $dateStart) && ($bookingDate <= $dateEnd) ) {

                $totalNumber += 1;
            }
        }

        return $totalNumber;
    }

    /**
     * Get company's orders amount from a date range
     *
     * @param \MongoId $companyId
     * @param int $dateStart
     * @param int $dateEnd
     *
     * @return array $totals
     */
    public function getCompanyOrdersAmountByDate($companyId, $dateStart, $dateEnd) {

        $criteria = Filter::set('references.company.$id', Filter::EQUAL, new \MongoId($companyId));
        $orders   = OrderMapper::find($criteria, 0);

        $totals = array(
            'amount' => 0,
            'number' => 0
        );
        foreach ($orders as $orderDetails) {

            $bookingEntity = $orderDetails->getField('references')->getField('booking');
            $bookingDate   = $this->orderApi->convertDateToTimestamp($bookingEntity->getField('schedule'));

            if ( ($bookingDate >= $dateStart) && ($bookingDate <= $dateEnd) ) {

                if ($bookingEntity->getField('selectedCarCombination')->getField('priceWithDiscount')) {
                    $totals['amount'] += $bookingEntity->getField('selectedCarCombination')->getField('priceWithDiscount');
                } else {
                    $totals['amount'] += $bookingEntity->getField('selectedCarCombination')->getField('price');
                }
                $totals['number'] += 1;
            }
        }

        return $totals;
    }

    /**
     * Get total income generated by company collection
     *
     * @param Collection $companies
     * @param int $dateStart
     * @param int $dateEnd
     *
     * @return int $totalIncome
     */
    public function getTotalIncome($companies, $dateStart, $dateEnd) {

        $totalIncome = 0;
        foreach ($companies as $companyEntity) {

            $totalCompanyAmount = $this->getCompanyOrdersAmountByDate($companyEntity->getField('_id'), $dateStart, $dateEnd);
            $totalIncome += $totalCompanyAmount['amount'];
        }

        return $totalIncome;
    }

    private function createDefaultDepartment($ownerEntity) {

        $department = new DepartmentEntity();

        $department->setField('name', $department::DEFAULT_DEPARTMENT_NAME);
        $department->setField($department::FIELD_READ_ONLY, true);
        $department->addUser(UserMapper::createReference($ownerEntity));

        return $department;
    }

    /**
     * PDF set style
     *
     * @param $pdfStyle
     * @param string $namespace
     *
     */
    private function setPdfStyles($pdfStyle, $namespace = '') {
        if (empty($namespace)) {
            $this->pdfStyles[] = $pdfStyle;
        } else {
            $this->pdfStyles[$namespace] = $pdfStyle;
        }
    }

    /**
     * PDF get style
     *
     * @param $namespace
     *
     * @return mixed
     */
    private function getPdfStyle($namespace) {
        return $this->pdfStyles[$namespace];
    }

    /**
     * PDF set page width
     *
     * @param $pageWidth
     *
     * @return mixed
     */
    private function setPageWidth($pageWidth) {
        return $this->pageWidth = $pageWidth;
    }

    /**
     * PDF get page width
     *
     * @return mixed
     */
    private function getPageWidth() {
        return $this->pageWidth;
    }

    /**
     * PDF set page height
     *
     * @param $pageHeight
     *
     * @return mixed
     */
    private function setPageHeight($pageHeight) {
        return $this->pageHeight = $pageHeight;
    }

    /**
     * PDF get page height
     *
     * @return mixed
     */
    private function getPageHeight() {
        return $this->pageHeight;
    }

    /**
     * PDF set logo-image width
     *
     * @param $logoImageWidth
     *
     * @return mixed
     */
    private function setLogoImageWidth($logoImageWidth) {
        return $this->logoImageWidth = $logoImageWidth;
    }

    /**
     * PDF get logo-image width
     *
     * @return mixed
     */
    private function getLogoImageWidth() {
        return $this->logoImageWidth;
    }

    /**
     * PDF set logo-image height
     *
     * @param $logoImageHeight
     *
     * @return mixed
     */
    private function setLogoImageHeight($logoImageHeight) {
        return $this->logoImageHeight = $logoImageHeight;
    }

    /**
     * PDF get logo-image height
     *
     * @return mixed
     */
    private function getLogoImageHeight() {
        return $this->logoImageHeight;
    }

    /**
     * PDF set page margin left
     *
     * @param $pageMarginleft
     *
     * @return mixed
     */
    private function setPageMarginleft($pageMarginleft) {
        return $this->pageMarginLeft = $pageMarginleft;
    }

    /**
     * PDF get page margin left
     *
     * @return mixed
     */
    private function getPageMarginLeft() {
        return $this->pageMarginLeft;
    }

    /**
     * PDF set language
     *
     * @param string @lang
     */
    private function pdfSetLanguage($lang) {

        if ($lang == "en") {
            $languageCode = "en_US";
        } else {
            $languageCode = "ro_RO";
        }
        putenv("LANG=" . $languageCode);
        setlocale(LC_ALL, $languageCode);
        bindtextdomain($lang, getcwd() . "/languages");
        textdomain($lang);
    }

    /**
     * PDF initialization
     */
    private function pdfInit(&$page) {

        $this->logo = \Zend_Pdf_Image::imageWithPath(__DIR__ . '/Resources/logo_print.png');

        $blackStyle = new \Zend_Pdf_Style();
        $blackStyle->setFillColor(new \Zend_Pdf_Color_Html('#000000'));

        $boldLine = new \Zend_Pdf_Style();
        $boldLine->setLineColor(new \Zend_Pdf_Color_Html('#d7d7d7'));
        $boldLine->setFillColor(new \Zend_Pdf_Color_Html('#d7d7d7'));
        $boldLine->setLineWidth(1);

        $this->setPdfStyles($blackStyle, 'blackStyle');
        $this->setPdfStyles($boldLine, 'boldLine');
        $this->setPageWidth($page->getWidth());
        $this->setPageHeight($page->getHeight());
        $this->setLogoImageWidth(280);
        $this->setLogoImageHeight(72);
        $this->setPageMarginleft(40);
    }

    /**
     * PDF set header
     */
    private function pdfSetHeader(&$page) {

        $page->drawImage($this->logo, 30, $this->getPageHeight()-$this->getLogoImageHeight()-15, $this->getLogoImageWidth()+30, $this->getPageHeight()-15);

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
            ->drawText('S.C. MLS S.R.L.', $this->getPageWidth()/2+70, $this->getPageHeight()-18)
            ->drawText('Cal. Dorobantilor, Bl. 9, Sc. B, Ap. 19', $this->getPageWidth()/2+70, $this->getPageHeight()-30)
            ->drawText('300298, Timisoara, Timis', $this->getPageWidth()/2+70, $this->getPageHeight()-42)
            ->drawText(getText('Reg. Com. ') . 'J35/1001/2004', $this->getPageWidth()/2+70, $this->getPageHeight()-54)
            ->drawText(getText('C.U.I.: ') . 'RO 16311704', $this->getPageWidth()/2+70, $this->getPageHeight()-66)
            ->drawText('Tel.: +40 256 212 489 / Fax: +40 372 895 991', $this->getPageWidth()/2+70, $this->getPageHeight()-78)
            ->drawText(getText('Banca: ') . 'UNICREDIT TIRIAC BANK', $this->getPageWidth()/2+70, $this->getPageHeight()-90)
            ->drawText('IBAN: RO61BACX0000000595790000', $this->getPageWidth()/2+70, $this->getPageHeight()-102);

        $page->setStyle($this->getPdfStyle('boldLine'))
            ->drawRectangle(15, 723, $this->getPageWidth()-15, $this->getPageHeight()-117);
        $page->setStyle($this->getPdfStyle('blackStyle'));
    }

    /**
     * PDF set footer
     *
     * @param string $pageNumber
     */
    private function pdfSetFooter(&$page, $pageNumber) {

        $page->setStyle($this->getPdfStyle('boldLine'))
            ->drawRectangle(15, 65, $this->getPageWidth()-15, $this->getPageHeight()-776);
        $page->setStyle($this->getPdfStyle('blackStyle'));

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
            ->drawText('Email: office@citylimo.ro, office@citycab.ro', $this->getPageMarginLeft(), $this->getPageHeight()-797)
            ->drawText('Website: www.citylimo.ro www.citycab.ro www.citycar.ro', $this->getPageMarginLeft(), $this->getPageHeight()-812);

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
            ->drawText($pageNumber, $this->getPageWidth()-30, $this->getPageHeight()-812);
    }

    /**
     * Set company information
     *
     * @param $page
     * @param $pdfData
     * @param $pageRemainingSpace
     */
    private function pdfSetCompanyInfo(&$page, $pdfData, &$pageRemainingSpace) {

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 12)
            ->drawText('Informatii companie', $this->getPageMarginLeft(), $pageRemainingSpace);

        $pageRemainingSpace -= 20;
        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 10)
            ->drawText('Nume: ' . $pdfData['companyInfo']['name'], $this->getPageMarginLeft(), $pageRemainingSpace)
            ->drawText('Proprietar: ' . $pdfData['companyInfo']['owner'], $this->getPageMarginLeft(), $pageRemainingSpace-15)
            ->drawText('Discount business hours: ' . $pdfData['companyInfo']['discount-bh'] . '%', $this->getPageMarginLeft(), $pageRemainingSpace-30)
            ->drawText('Discount extra-business hours: ' . $pdfData['companyInfo']['discount-ebh'] . '%', $this->getPageMarginLeft(), $pageRemainingSpace-45)
            ->drawText('Nr. departamente: ' . $pdfData['companyInfo']['nrDept'], $this->getPageMarginLeft(), $pageRemainingSpace-60);
    }

    /**
     * Set departments information
     *
     * @param $page
     * @param $pdfData
     * @param $pageRemainingSpace
     */
    private function pdfSetDepartmentsInfo(&$page, $pdfData, &$pageRemainingSpace) {

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 12)
            ->drawText('Departamente', $this->getPageMarginLeft(), $pageRemainingSpace);

        $pageRemainingSpace -= 30;
        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 11)
            ->drawText('Nume', $this->getPageMarginLeft(), $pageRemainingSpace)
            ->drawText('Numar comenzi', $this->getPageWidth()/2+30, $pageRemainingSpace)
            ->drawText('Venit total', $this->getPageWidth()/2+195, $pageRemainingSpace);

        $page->setStyle($this->getPdfStyle('boldLine'))
            ->drawRectangle($this->getPageMarginLeft()-5, $pageRemainingSpace-10, $this->getPageWidth()-$this->getPageMarginLeft()+5, $pageRemainingSpace-11);
        $page->setStyle($this->getPdfStyle('blackStyle'));

        $pageRemainingSpace -= 30;

        // List of departments
        foreach ($pdfData['departments'] as $department) {

            $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 10)
                ->drawText($department['name'], $this->getPageMarginLeft(), $pageRemainingSpace)
                ->drawText($department['nrOrders'], $this->getPageWidth()/2+60, $pageRemainingSpace)
                ->drawText(number_format($department['amount'], 2) . ' EUR', $this->getPageWidth()/2+195, $pageRemainingSpace);
            $pageRemainingSpace -= 20;
        }
    }

    /**
     * Set department header
     *
     * @param $page
     * @param $departmentDetails
     * @param $pageRemainingSpace
     */
    private function pdfSetDepartmentHeader(&$page, $departmentDetails, &$pageRemainingSpace) {

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 12)
            ->drawText($departmentDetails['orders'][0]['deptName'], $this->getPageMarginLeft(), $pageRemainingSpace);

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 10)
            ->drawText('Comenzi: ', $this->getPageWidth()/2+20, $pageRemainingSpace);
        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 11)
            ->drawText($departmentDetails['nrOrders'], $this->getPageWidth()/2+70, $pageRemainingSpace);

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 10)
            ->drawText('Total venit: ', $this->getPageWidth()/2+120, $pageRemainingSpace);
        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 12)
            ->drawText(number_format($departmentDetails['totalAmount'], 2) . ' EUR', $this->getPageWidth()/2+175, $pageRemainingSpace);

        $pageRemainingSpace -= 50;
    }

    /**
     * Set department header for order-listing
     *
     * @param $page
     * @param $pageRemainingSpace
     */
    private function pdfSetDepartmentListingHeader(&$page, &$pageRemainingSpace) {

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 10)
            ->drawText('Comanda #', $this->getPageMarginLeft(), $pageRemainingSpace)
            ->drawText('Tip serviciu', $this->getPageMarginLeft()+80, $pageRemainingSpace)
            ->drawText('Clasa masina', $this->getPageMarginLeft()+160, $pageRemainingSpace)
            ->drawText('Nume client', $this->getPageMarginLeft()+240, $pageRemainingSpace)
            ->drawText('Data', $this->getPageWidth()/2+100, $pageRemainingSpace)
            ->drawText('Pret', $this->getPageWidth()/2+200, $pageRemainingSpace);

        $page->setStyle($this->getPdfStyle('boldLine'))
            ->drawRectangle($this->getPageMarginLeft()-5, $pageRemainingSpace-10, $this->getPageWidth()-$this->getPageMarginLeft()+5, $pageRemainingSpace-11);
        $page->setStyle($this->getPdfStyle('blackStyle'));

        $pageRemainingSpace -= 30;
    }


    /**
     * Generate company PDF for export
     *
     * @param $pdfData
     * @param $language
     * @param bool $downloadPdf
     *
     * @return string
     */
    public function generatePdfForCompany($pdfData, $language, $downloadPdf = false) {

        $this->pdfSetLanguage($language);

        $pdf  = new \Zend_Pdf();
        $page = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);

        $this->pdfInit($page);
        $this->pdfSetHeader($page);

        $pageRemainingSpace = 690;

        $this->pdfSetCompanyInfo($page, $pdfData, $pageRemainingSpace);

        // Date range
        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 10)
            ->drawText('Data:', $this->getPageWidth()/2+30, $pageRemainingSpace+20);
        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 10)
            ->drawText($pdfData['date']['from'] . ' - ' . $pdfData['date']['to'], $this->getPageWidth()/2+60, $pageRemainingSpace+20);

        // Total income
        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 13)
            ->drawText('Venit total:', $this->getPageWidth()/2+30, $pageRemainingSpace-25)
            ->drawText('Nr. comenzi:', $this->getPageWidth()/2+30, $pageRemainingSpace-50);

        $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA_BOLD), 15)
            ->drawText(number_format($pdfData['totalIncome'], 2) . ' EUR', $this->getPageWidth()/2+110, $pageRemainingSpace-25)
            ->drawText($pdfData['ordersAmount'], $this->getPageWidth()/2+110, $pageRemainingSpace-50);

        $pageRemainingSpace -= 120;

        $this->pdfSetDepartmentsInfo($page, $pdfData, $pageRemainingSpace);

        $this->pdfSetFooter($page, $this->pageNumber);

        $pdf->pages[] = $page;


        /** Department orders */
        $this->pageNumber = 2;
        foreach ($pdfData['departmentsDetails'] as $departmentDetails) {

            $page = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);

            $this->pdfSetHeader($page);

            $pageRemainingSpace = 680;

            $this->pdfSetDepartmentHeader($page, $departmentDetails, $pageRemainingSpace);

            $this->pdfSetDepartmentListingHeader($page, $pageRemainingSpace);

            // Department orders listing
            foreach ($departmentDetails['orders'] as $key => $departmentOrder) {

                $page->setFont(\Zend_Pdf_Font::fontWithName(\Zend_Pdf_Font::FONT_HELVETICA), 9)
                    ->drawText($departmentOrder['uniqueId'], $this->getPageMarginLeft()+15, $pageRemainingSpace)
                    ->drawText($departmentOrder['serviceType'], $this->getPageMarginLeft()+80, $pageRemainingSpace)
                    ->drawText($departmentOrder['carType'], $this->getPageMarginLeft()+160, $pageRemainingSpace)
                    ->drawText($departmentOrder['name'], $this->getPageMarginLeft()+240, $pageRemainingSpace)
                    ->drawText($departmentOrder['scheduleDate'], $this->getPageWidth()/2+100, $pageRemainingSpace)
                    ->drawText(number_format($departmentOrder['price'], 2) . ' EUR', $this->getPageWidth()/2+200, $pageRemainingSpace);

                $pageRemainingSpace -= 15;

                if (($key+1) % self::ORDERS_PER_PAGE == 0) {

                    $this->pdfSetFooter($page, $this->pageNumber);
                    $pdf->pages[] = $page;
                    $this->pageNumber++;

                    // New department order-listing page
                    $page = $pdf->newPage(\Zend_Pdf_Page::SIZE_A4);

                    $this->pdfSetHeader($page);

                    $pageRemainingSpace = 690;

                    $this->pdfSetDepartmentHeader($page, $departmentDetails, $pageRemainingSpace);

                    $this->pdfSetDepartmentListingHeader($page, $pageRemainingSpace);
                }

            }

            $this->pdfSetFooter($page, $this->pageNumber);

            $pdf->pages[] = $page;

            $this->pageNumber++;
        }

        if ($downloadPdf) {
            header('Content-type: application/pdf; charset=utf-8');
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename="Company Report.pdf"');
            header('Content-Transfer-Encoding: binary');
            header('Connection: Keep-Alive');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
        } else {
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="Company Report.pdf"');
            header('Content-Transfer-Encoding: binary');
            header('Accept-Ranges: bytes');
        }

        return $pdf->render();
    }

}
?>