<?php

namespace CL\Company\Mapper;

use \Xeeo\Services\Database\Mongo\Mapper as Mapper;

class Company extends Mapper {

    protected static $collection = 'companies';
    protected static $entity     = '\CL\Company\Entity\Company';

}
?>