<?php

namespace CL\Car\Mapper;

use \Xeeo\Services\Database\Mongo\Mapper as Mapper;

final class Car extends Mapper {

    protected static $collection = 'carTypes';
    protected static $entity     = '\CL\Car\Entity\Car';
}
?>