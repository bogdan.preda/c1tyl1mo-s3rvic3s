<?php

namespace CL\Car\Mapper\Car;

use \Xeeo\Services\Database\Mongo\Mapper as Mapper;

final class Type extends Mapper {

    protected static $collection = 'carTypes';
    protected static $entity     = '\CL\Car\Entity\Car\Type';
}
?>