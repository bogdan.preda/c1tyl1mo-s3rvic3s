<?php

namespace CL\Car\Entity\Car;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class Prices extends AbstractEntity {

    public function initFields() {
        return array(
            "startTax"             => $this->field()
                    ->setRequired(false),
            "perHour"             => $this->field()
                    ->setRequired(false),
            "businessHoursPerKm"      => $this->field()
                    ->setRequired(false),
            "extraBusinessHoursPerKm" => $this->field()
                    ->setRequired(false)
        );
    }
}
?>
