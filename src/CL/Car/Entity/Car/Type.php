<?php

namespace CL\Car\Entity\Car;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class Type extends AbstractEntity {

    public function initFields() {
        return array(
            "_id"                => $this->field()
                    ->setIgnore(true),
            "type"               => $this->field()
                    ->setRequired(true),
            "availableCars"      => $this->field()
                    ->setRequired(true),
            "prices"             => $this->field()
                    ->setRequired(true)
                    ->setType('CL\Car\Entity\Car\Prices'),
            "numberOfPassengers" => $this->field()
                    ->setRequired(true),
            "description"        => $this->field()
                    ->setRequired(true)
                    ->setType('CL\Car\Entity\Car\Description'),
            "minDistanceLimit"   => $this->field()
                    ->setValue(20)
                    ->setRequired(true),
            "sort" => $this->field()
                    ->setRequired(true),
            "createdAt"          => $this->field()
                    ->setRequired(true)
        );
    }
}
?>