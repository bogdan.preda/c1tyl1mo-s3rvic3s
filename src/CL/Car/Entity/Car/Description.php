<?php

namespace CL\Car\Entity\Car;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class Description extends AbstractEntity {

    public function initFields() {
        return array(
            "ro" => $this->field()
                    ->setRequired(false),
            "en" => $this->field()
                    ->setRequired(false),
        );
    }
}
?>