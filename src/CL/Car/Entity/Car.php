<?php

namespace CL\Car\Entity;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class Car extends AbstractEntity {

    const ROUTE_DIRECT_TRANSFER = 'direct-transfer';
    const ROUTE_MULTIPLE_STOPS  = 'multiple-stops';
    const ROUTE_RENT_PER_HOUR   = 'rent-per-hour';
    const ROUTE_FROM_AIRPORT    = 'from-airport';
    const ROUTE_TO_AIRPORT      = 'to-airport';
    
    public function initFields() {
        return array(
            "__v"                => $this->field()
                    ->setIgnore(true),
            "_id"                => $this->field()
                    ->setIgnore(true),
            "type"               => $this->field()
                    ->setRequired(true),
            "availableCars"      => $this->field()
                    ->setRequired(true),
            "prices"             => $this->field()
                    ->setRequired(true)
                    ->setType('CL\Car\Entity\Car\Prices'),
            "numberOfPassengers" => $this->field()
                    ->setRequired(true),
            "description"        => $this->field()
                    ->setRequired(true)
                    ->setType('CL\Car\Entity\Car\Description'),
            "minDistanceLimit"   => $this->field()
                    ->setValue(20)
                    ->setRequired(true),
            "sort" => $this->field()
                    ->setRequired(true),
            "createdAt"          => $this->field()
                    ->setRequired(true)
        );
    }
}
?>