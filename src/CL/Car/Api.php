<?php

namespace CL\Car;

use CL\Config\Mapper\Config;
use \CL\Core\ApiAbstract,
    \CL\Car\Entity\Car as CarEntity,
    \CL\Car\Entity\Car\Prices as CarPricesEntity,
    \CL\Car\Mapper\Car as CarMapper,
    \CL\Core\Registry,
    \CL\Config\Api as ConfigApi,
    \CL\Company\Entity\Company\Discount as DiscountEntity,
    \Xeeo\Services\Database\Mongo\Filter,
    \IOEntities\Booking,
    \IOEntities\Booking\Schedule;

class Api extends ApiAbstract {

    const ITEMS_PER_PAGE = 10;

    /**
     * @var $configApi ConfigApi
     */
    private $configApi;

    public function init() {

        parent::init();
        $this->configApi = ConfigApi::getInstance();
    }

    /**
     * Returns collection of cars by a given filter
     *
     * @param array $criteria
     * @param array $sort
     * @param int   $page
     * @param int   $itemsPerPage
     *
     * @return \ArrayObject
     */
    public function findCarsBy($criteria, $sort = array(), $page = 1, $itemsPerPage = self::ITEMS_PER_PAGE)
    {
        $itemsPerPage = (empty($itemsPerPage)) ? $itemsPerPage : abs((int) $itemsPerPage);
        $page         = (empty($page)) ? 1 : abs((int) $page);
        $offset       = ($page - 1) * $itemsPerPage;

        if (false === is_array($sort)) {
            $sort = array();
        }

        $result = CarMapper::find($criteria, $itemsPerPage, $offset, $sort);

        return $result;
    }

    public function getCarTypes() {

        $result = CarMapper::find(null, 0);

        return $result;
    }

    public function saveCarType($carTypeEntity) {
        return CarMapper::save($carTypeEntity);
    }

    /**
     * Save a car in DB
     *
     * @param array $car
     */
    public function addCar($car) {

        $CarEntity = new CarEntity();
        $CarEntity->setField('type', $car['type']);
        $CarEntity->setField('availableCars', $car['availableCars']);
        $CarEntity->setField('numberOfPassengers', $car['passengers']);
        $CarEntity->setField('description', $car['description']);
        $CarEntity->setField('createdAt', new \MongoDate(time()));

        $CarPricesEntity = new CarPricesEntity();
        $CarPricesEntity->setField('startTax', floatval($car['startTax']));
        $CarPricesEntity->setField('businessHoursPerKm', floatval($car['bh']));
        $CarPricesEntity->setField('extraBusinessHoursPerKm', floatval($car['ebh']));
        $CarPricesEntity->setField('perHour', floatval($car['hour']));


        $CarEntity->setField('prices', $CarPricesEntity);

        $result = CarMapper::save($CarEntity);

        if (!$result) {
            return false;
        }

        return true;
    }

    /**
     * Edit a car in DB
     *
     * @param array $Car
     */
    public function editCar($CarEntity) {

        $result = CarMapper::save($CarEntity);

        if (!$result) {
            return false;
        }

        return true;
    }

    private function isInBusinessHours($schedule) {

        $bookingHour   = $schedule->getField('hour');
        $bookingMinute = $schedule->getField('minute');
        $pickupTime    = date('Hi', mktime($bookingHour, $bookingMinute));

        return (($pickupTime >= "0900") && ($pickupTime <= "1700"));
    }

    private function getUsefulDistance(Booking $booking, $minDistance = 0) {

        $routeType      = $booking->getField('routeType');
        $effort         = $booking->getField('effort');

        if (is_null($effort)) {
            $routeDistance = 0;
        } else {
            $routeDistance  = $effort->getField('distance') / 1000; // kilometers
        }

        $usefulDistance = ($routeDistance < $minDistance) ? $minDistance : $routeDistance;

        return $usefulDistance;
    }

    private function getPrice($schedule, $carTypePrices) {

        if ($this->isInBusinessHours($schedule)) {
            $pricePerKm = $carTypePrices->getField('businessHoursPerKm');
        } else {
            $pricePerKm = $carTypePrices->getField('extraBusinessHoursPerKm');
        }
        return $pricePerKm;
    }

    public function calculateDiscount($price = 0, Schedule $schedule, DiscountEntity $discountEntity) {

        $discount = $this->getDiscountAmount($schedule, $discountEntity);

        $priceWithDiscount = $price - ( $price * ( $discount / 100 ) );
        $priceWithDiscount = ceil($priceWithDiscount);

        return $priceWithDiscount;
    }

    public function getDiscountAmount($schedule, $discountEntity) {

        if ($this->isInBusinessHours($schedule)) {
            return $discountEntity->getField('businessHours');
        } else {
            return $discountEntity->getField('extraBusinessHours');
        }
    }

    public function calculatePrice(Booking $booking, $schedule, $nrOfCars, $carTypePrices, $minDistance) {

        $configEntity = $this->configApi->getConfigEntity();
        $startFee   = (!empty($carTypePrices->getField('startTax'))) ? $carTypePrices->getField('startTax') : $configEntity->getPriceRates()->getStartTax();
        $vatFee     = $configEntity->getPriceRates()->getVatTax(); // $config['priceRates']['vatTax'];
        $vatFee     = 1 + ($vatFee / 100);
        $routeType  = $booking->getField('routeType');
        $distance   = $this->getUsefulDistance($booking, $minDistance);
        $pricePerKm = $this->getPrice($schedule, $carTypePrices);


        $airportTax = $booking->getField('applyAirportTax') == 1 ? $configEntity->getPriceRates()->getAirportTax() : 0;
        switch ($routeType) {
            case 'direct-transfer' :
                $pricePerCar = $this->calculatePriceForDirectTransfer($distance, $pricePerKm) + $airportTax;
                break;
            case 'multiple-stops' :
                $stopFee     = $configEntity->getPriceRates()->getWaypointTax(); // $config['priceRates']['waypointTax'];
                $nrOfStops   = count($booking->getField('waypoints'));
                $pricePerCar = $this->calculatePriceForMultipleStops($stopFee, $nrOfStops, $distance, $pricePerKm) + $airportTax;
                break;
            case 'rent-per-hour' :
                $pricePerHour = $carTypePrices->getField('perHour');
                $duration     = round($booking->getField('effort')->getField('duration') / 3600); // hours
                $pricePerCar  = $this->calculatePriceForHourRent($duration, $pricePerHour) + $airportTax;
                break;
            case 'from-airport' :
            case 'to-airport'   :
                $airportTax  = $configEntity->getPriceRates()->getAirportTax(); // $config['priceRates']['airportTax'];
                $pricePerCar = $this->calculatePriceForAirport($airportTax, $distance, $pricePerKm);
                break;
        }

        $price = ($startFee + $pricePerCar) * $nrOfCars;
        $price = $price * $vatFee;
        $price = ceil($price);

        return $price;

    }

    private function calculatePriceForDirectTransfer($distance, $pricePerKm) {

        $price = $distance * $pricePerKm;

        return $price;
    }

    private function calculatePriceForMultipleStops($stopFee, $nrOfStops, $distance, $pricePerKm) {

        $price = ( $nrOfStops * $stopFee ) + ( $distance * $pricePerKm );

        return $price;
    }

    private function calculatePriceForHourRent($duration, $pricePerHour) {

        $price = $duration * $pricePerHour;

        return $price;
    }

    private function calculatePriceForAirport($airportTax, $distance, $pricePerKm) {

        $price = $airportTax + ( $distance * $pricePerKm );

        return $price;
    }

}
?>
