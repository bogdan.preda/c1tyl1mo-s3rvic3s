<?php

namespace CL\User;

use \CL\Core\ApiAbstract as ApiAbstract,
    \CL\User\Entity\User as UserEntity,
    \CL\User\Entity\User\Company as UserCompanyEntity,
    \CL\User\Entity\User\PersonalInformation,
    \CL\User\Mapper\User as UserMapper,
    \CL\Company\Entity\Company\Department as DepartmentEntity,
    \CL\Company\Entity\Company\Departments as DepartmentsEntity,
    \CL\Company\Api as CompanyApi,
    \CL\Company\Mapper\Company as CompanyMapper,
    \Xeeo\Services\Authenticate\AuthenticateApi,
    \Xeeo\Services\Database\Mongo\Filter,
    \Xeeo\Services\Database\Mongo\Group;

class Api extends ApiAbstract {

    const ITEMS_PER_PAGE = 10;

    const SORT_DESCENDING = 'DESC';
    const SORT_ASCENDING  = 'ASC';

    /** @var  AuthenticateApi $authenticateApi */
    private $authenticateApi;

    /** @var  UserEntity $userEntity */
    private $currentUser;

    /** @var  CompanyApi $companyApi */
    private $companyApi;

    public function init() {

        parent::init();
        $this->authenticateApi = AuthenticateApi::getInstance();
        $this->companyApi      = CompanyApi::getInstance();
        $this->initCurrentUser();
    }

    public function login($identifier, $password) {
                
        $user = $this->getUserByEmail($identifier);

        if (false === is_null($user)) {
            switch ($user->getField('status')) {
                case $user::STATUS_DELETED : throw new \Exception('User account was deleted', 998);
                    break;
            }
        }

        try {
            $this->authenticateApi->setIdentifierValue($identifier)
                ->setPasswordValue($password)
                ->ignorePassword(false)
                ->authenticate();
        } catch(\Exception $e) {
            throw new \Exception($e->getMessage(), 401);
        }

        $this->initCurrentUser();

        return true;
    }

    public function register(&$registerEntity) {

        $user = $this->getUserByEmail($registerEntity->getField('email'));

        if (!is_null($user)) {
            throw new \Exception("Email already used with another account.", 996);
        }

        $accountType = UserEntity::TYPE_USER;
        if (!is_null($registerEntity->getField('companyName'))) {
            $accountType = UserEntity::TYPE_COMPANY_OWNER;

            $criteriaCompany  = Filter::set('name', Filter::EQUAL, $registerEntity->getField('companyName'));
            $company          = CompanyMapper::findOne($criteriaCompany);

            if (!is_null($company)) {
                throw new \Exception("This company already has a CityLimo<sup>&reg;</sup> owner.", 401);
            }
        }

        if (!is_null($registerEntity->getField('departmentId'))) {
            $accountType = UserEntity::TYPE_COMPANY_USER;
        }

        try {
            $skipLogin = false;
            switch ($accountType) {
                case UserEntity::TYPE_USER          : $this->createUserFromRegister($registerEntity);
                                                      break;
                case UserEntity::TYPE_COMPANY_OWNER : $this->createCompanyOwnerUserFromRegister($registerEntity, $accountType); 
                                                      break;
                case UserEntity::TYPE_COMPANY_USER  : $this->createCompanyUser($registerEntity, $accountType);
                                                      $skipLogin = true;
                                                      break;
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), $e->getCode());
        }

        // Auto login
        if (false === $skipLogin) {
            $this->login($registerEntity->getField('email'), $registerEntity->getField('password'));
        }
        return true;
    }

    public function initCurrentUser()
    {
        $auth              = \Zend_Auth::getInstance();
        $this->currentUser = new UserEntity();

        if ($auth->hasIdentity()) {
            $identity = $auth->getIdentity();
            $user     = UserMapper::get($identity);
            if (!empty($user)) {
                $this->currentUser = $user;
            }
        }

        return true;
    }

    public function getCurrentUser()
    {
        if (is_null($this->currentUser)) {
            $this->initCurrentUser();
        }

        return $this->currentUser;
    }

    public function logout()
    {
        \Zend_Auth::getInstance()->clearIdentity();

        $this->initCurrentUser();

        return true;
    }

    public function isLoggedIn() {

        return (false === is_null($this->currentUser->getField('_id')));
    }

    public function isGuest() {

        return (false === $this->isLoggedIn());
    }

    public function isCompanyOwner() {

        if (!is_null($this->currentUser->getField('company'))) {
            return (bool)($this->currentUser->getField('company')->getField('role') == UserEntity::TYPE_COMPANY_OWNER);    
        }
        return false;
    }
    
    public function isCompanyAccount() {
        return (false === is_null($this->currentUser->getField('company'))); 
    }

    public function validateCurrentPassword($password) {

        return $this->authenticateApi->validatePassword($password, $this->currentUser->getField('password'));
    }

    public function hashPassword($password) {

        return $this->authenticateApi->hashPassword($password);
    }
    
    public function generateRandomPassword($length = 10) {
        
        return $this->authenticateApi->generatePassword($length);
    }

    private function createUserEntityFromRegister($registerEntity) {

        $userEntity         = new UserEntity();
        $personalInfoEntity = new PersonalInformation();

        $userEntity->setField('company', $registerEntity->getField('company'));
        $userEntity->setField('firstName', $registerEntity->getField('firstName'));
        $userEntity->setField('lastName', $registerEntity->getField('lastName'));
        $userEntity->setField('email', $registerEntity->getField('email'));
        $userEntity->setField('status', 'active');
        $userEntity->setField('password', $this->authenticateApi->hashPassword($registerEntity->getField('password')));
        $personalInfoEntity->setField('phone', $registerEntity->getField('phone'));
        $personalInfoEntity->setField('address', $registerEntity->getField('address'));
        $userEntity->setField('personalInformation', $personalInfoEntity);
        $userEntity->setField('createdAt', new \MongoDate(time()));

        return $userEntity;
    }

    /**
     * Returns collection of users by a given filter
     *
     * @param array $criteria
     * @param array $sort
     * @param int   $page
     * @param int   $itemsPerPage
     *
     * @return \ArrayObject
     */
    public function findUsersBy($criteria, $sort = array(), $page = 1, $itemsPerPage = self::ITEMS_PER_PAGE)
    {
        $itemsPerPage = (empty($itemsPerPage)) ? $itemsPerPage : abs((int) $itemsPerPage);
        $page         = (empty($page)) ? 1 : abs((int) $page);
        $offset       = ($page - 1) * $itemsPerPage;

        if (false === is_array($sort)) {
            $sort = array();
        }

        $result = UserMapper::find($criteria, $itemsPerPage, $offset, $sort);

        return $result;
    }

    /**
     * Return an user entity by id
     *
     * @param \MongoId|string $id
     *
     * @return UserEntity|null
     *
     * @throws \Exception
     */
    public function getUser($id)
    {
        $result = UserMapper::get($id);

        return $result;
    }

    /**
     * Get the user entity where his company id equals the given company id
     *
     * @param \MongoId $companyId
     *
     * @return UserEntity $result
     */
    public function getUserIfCompanyOwner($companyId) {

        $criteriaCompanyId   = Filter::set('company.reference.$id', Filter::EQUAL, new \MongoId($companyId));
        $criteriaCompanyRole = Filter::set('company.role', Filter::EQUAL, 'company-owner');
        $criteria            = Group::set($criteriaCompanyId, Group::OPERATOR_AND, $criteriaCompanyRole);
        $result              = UserMapper::findOne($criteria);

        return $result;
    }

    /**
     * @param $emailAddress
     *
     * @return UserEntity|null
     */
    public function getUserByEmail($emailAddress) {
        $criteriaEmail = Filter::set('email', Filter::EQUAL, $emailAddress);
        $user          = UserMapper::findOne($criteriaEmail);

        return $user;
    }

    /**
     * @param $userEntity
     * @param $newEmail
     *
     * @return UserEntity
     * @throws \Exception
     */
    public function updateUserEmail($userEntity, $newEmail) {

        if ($userEntity->getField('email') != $newEmail) {

            $user = $this->getUserByEmail($newEmail);

            if (!is_null($user)) {
                throw new \Exception("Email already used with another account.", 996);
            }
            $userEntity->setField('email', $newEmail);
        }
        return $userEntity;
    }

    private function createUserFromRegister($registerEntity) {

        // Save user
        $userEntity = $this->createUserEntityFromRegister($registerEntity);
        UserMapper::save($userEntity);

        return $userEntity;
    }

    private function createCompanyOwnerUserFromRegister($registerEntity, $accountType) {

        $userEntity    = $this->createUserFromRegister($registerEntity);
        $companyEntity = $this->companyApi->createCompanyEntityFromRegister($registerEntity, $userEntity);

        CompanyMapper::save($companyEntity);

        $userCompanyEntity = new UserCompanyEntity();
        $userCompanyEntity->setField($userCompanyEntity::FIELD_ROLE, $accountType);
        $userCompanyEntity->setField($userCompanyEntity::FIELD_REFERENCE, $companyEntity);
        $userCompanyEntity->setField($userCompanyEntity::FIELD_DEPARTMENT, DepartmentEntity::DEFAULT_DEPARTMENT_NAME);
        $userCompanyEntity->setField($userCompanyEntity::FIELD_DEPARTMENT_ID, 
            $companyEntity->getDepartmentsInfo()->getField(DepartmentsEntity::FIELD_DEFAULT_DEPARTMENT_ID));

        $userEntity->setField('company', $userCompanyEntity);
        UserMapper::save($userEntity);

        return $userEntity;
    }

    private function createCompanyUser(&$registerEntity, $accountType) {

        if ($this->isGuest() || (false === $this->isCompanyOwner())) {
            throw new \Exception('Company users can only be created by company owners', 401);
        }
        
        $registerEntity->setField('password', $this->generateRandomPassword(8));

        $userEntity         = $this->createUserFromRegister($registerEntity);
        $userReference      = UserMapper::createReference($userEntity);
        $companyEntity      = $this->getCurrentUser()->getField('company')->getField('reference');
        $companyDepartments = $companyEntity->getField('departmentsInformation');

        $departmentId      = $registerEntity->getField('departmentId');

        $companyDepartmentEntity = $companyDepartments->getDepartmentById($departmentId);
        $companyDepartmentEntity->addUser($userReference);
        $companyDepartments->addDepartment($companyDepartmentEntity);

        $userCompanyEntity = new UserCompanyEntity();
        $userCompanyEntity->setField(
            $userCompanyEntity::FIELD_DEPARTMENT_ID,
            $companyDepartmentEntity->getField($companyDepartmentEntity::FIELD_ID)
        );
        $userCompanyEntity->setField($userCompanyEntity::FIELD_ROLE, $accountType);
        $userCompanyEntity->setField($userCompanyEntity::FIELD_DEPARTMENT, $companyDepartmentEntity->getField('name'));
        $userCompanyEntity->setField($userCompanyEntity::FIELD_REFERENCE, $companyEntity);

        $userEntity->setField('company', $userCompanyEntity);
        UserMapper::save($userEntity);

        $companyEntity->setField('departmentsInformation', $companyDepartments);

        CompanyMapper::save($companyEntity);

        return $userEntity;
    }
}
?>