<?php

namespace CL\User\Mapper;

use \Xeeo\Services\Database\Mongo\Mapper as Mapper;

final class User extends Mapper {

    protected static $collection = 'users';
    protected static $entity     = '\CL\User\Entity\User';

}
?>