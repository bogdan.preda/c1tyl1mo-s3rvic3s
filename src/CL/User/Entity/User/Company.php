<?php

namespace CL\User\Entity\User;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity,
    \CL\Company\Mapper\Company as CompanyMapper;

class Company extends AbstractEntity {

    const FIELD_ROLE          = 'role';
    const FIELD_REFERENCE     = 'reference';
    const FIELD_DEPARTMENT    = 'departmentName';
    const FIELD_DEPARTMENT_ID = 'departmentId';

    public function initFields() {
        return array(
            self::FIELD_ROLE       => $this->field()
                    ->setRequired(false),
            self::FIELD_REFERENCE  => $this->field()
                    ->setReference(new CompanyMapper())
                    ->setRequired(false)
                    ->setIgnore(true),
            self::FIELD_DEPARTMENT => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false),
            self::FIELD_DEPARTMENT_ID => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false)
        );
    }
}
?>
