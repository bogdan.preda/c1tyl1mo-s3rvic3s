<?php

namespace CL\User\Entity\User;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class PersonalInformation extends AbstractEntity {

    public function initFields() {
        return array(
            "address" => $this->field()
                    ->setRequired(false),
            "phone" => $this->field()
                    ->setRequired(false)
        );
    }
}
?>
