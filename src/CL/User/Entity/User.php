<?php

namespace CL\User\Entity;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity;

class User extends AbstractEntity {

    const STATUS_ACTIVE  = 'active';
    const STATUS_DELETED = 'deleted';
    
    const TYPE_USER = 'user';
    const TYPE_COMPANY_OWNER = 'company-owner';
    const TYPE_COMPANY_USER  = 'company-user';
    
    public function initFields() {
        return array(
            "__v"                => $this->field()
                    ->setIgnore(true),
            "_id"                 => $this->field()
                    ->setIgnore(true),
            "token"               => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false),

            "tokenCard" => $this->field()
                ->setIgnore(true)->setRequired(false),
            "tokenExpirationDate" => $this->field()
                ->setIgnore(true)->setRequired(false),
            "panMasked" => $this->field()
                ->setIgnore(true)->setRequired(false),
            "company"             => $this->field()
                    ->setIgnore(true)
                    ->setRequired(false)
                    ->setType('CL\User\Entity\User\Company'),
            "firstName"           => $this->field()
                    ->setRequired(true),
            "lastName"            => $this->field()
                    ->setRequired(false),
            "email"               => $this->field()
                    ->setRequired(true),
            "password"            => $this->field()
                    ->setRequired(true),
            "status"              => $this->field()
                    ->setRequired(true),
            "personalInformation" => $this->field()
                    ->setRequired(false)

                    ->setType('CL\User\Entity\User\PersonalInformation'),
            "createdAt"           => $this->field()
                    ->setRequired(false)
        );
    }
}
?>