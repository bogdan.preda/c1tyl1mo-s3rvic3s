<?php

namespace CL\Voucher\Entity;

use \Xeeo\Services\Core\Abstracts\Entity as AbstractEntity,
    \CL\Company\Mapper\Company as CompanyMapper;

class Voucher extends AbstractEntity {
   
    public function initFields() {
        return array(
            "__v"                => $this->field()
                    ->setIgnore(true),
            "_id"            => $this->field()
                    ->setIgnore(true),
            "name"           => $this->field()
                    ->setRequired(true),
            "code"           => $this->field()
                    ->setRequired(true),
            "company"        => $this->field()
                    ->setReference(new CompanyMapper())
                    ->setRequired(false)
                    ->setIgnore(true),
            "discount"       => $this->field()
                    ->setRequired(true),
            "discountMethod" => $this->field()
                    ->setRequired(true),
            "startDate"      => $this->field()
                    ->setRequired(true),
            "endDate"        => $this->field()
                    ->setRequired(true),
            "usageLimit"     => $this->field()
                    ->setValue(100)
                    ->setRequired(true),
            "status"         => $this->field()
                    ->setRequired(true),
            "createdAt"      => $this->field()
                    ->setRequired(true)
        );
    }

    public function getId() {
        return $this->getField(self::FIELD_ID);
    }
}
?>
