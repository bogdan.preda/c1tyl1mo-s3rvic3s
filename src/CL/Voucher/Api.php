<?php

namespace CL\Voucher;

use \CL\Core\ApiAbstract,
    \CL\Voucher\Mapper\Voucher as VoucherMapper,
    \CL\Voucher\Entity\Voucher as VoucherEntity,
    \CL\Company\Api as CompanyApi,
    \Xeeo\Services\Database\Mongo\Filter;

class Api extends ApiAbstract {
    
    const ITEMS_PER_PAGE  = -1;
    
    /**
     * @var CompanyApi
     */
    private $companyApi;
    
    public function init() {
        parent::init();
        
        $this->companyApi = CompanyApi::getInstance();
    }

    /**
     * Returns collection of vouchers by a given filter
     *
     * @param array $criteria
     * @param array $sort
     * @param int   $page
     * @param int   $itemsPerPage
     *
     * @return \ArrayObject
     */
    public function findVouchersBy($criteria, $sort = array(), $page = 1, $itemsPerPage = null)
    {
        
        $itemsPerPage = (empty($itemsPerPage)) ? self::ITEMS_PER_PAGE : $itemsPerPage;
        $offset       = ($itemsPerPage > 0) ? ($page - 1) * $itemsPerPage : 0;
        $page         = (empty($page)) ? 1 : abs((int) $page);
        
        if (false === is_array($sort)) {
            $sort = array();
        }

        $result = VoucherMapper::find($criteria, $itemsPerPage, $offset, $sort);

        return $result;
    }
    
    public function findVoucherByCode($code) {
        $filter = Filter::set('code', Filter::EQUAL, $code);
        
        return VoucherMapper::findOne($filter);
    }
    
    public function validateVoucher($voucherEntity, $currentUser) {
        $now = new \MongoDate();
        
        if (empty($voucherEntity)) {
            throw new \Exception('Invalid Voucher', 800);
        }
        
        if ($voucherEntity->getField('status') !== 'active') {
            throw new \Exception('Invalid Voucher', 800);
        }

        if (!empty($voucherEntity->getField('company'))) {
            $voucherCompanyId     = $voucherEntity->getField('company')->getField('_id')->__toString();
            $currentUserCompanyId = $currentUser->getField('company')->getField('reference')->getField('_id')->__toString();

            if ($voucherCompanyId !== $currentUserCompanyId) {
                throw new \Exception('This Voucher is not assigned to your Company', 801);
            }
        }
        
        if (!(($voucherEntity->getField('startDate') <= $now) && 
            ($voucherEntity->getField('endDate') >= $now))) {
            throw new \Exception('Voucher is expired', 802);
        }
            
        return true;
    }
    
    public function applyVoucherDiscount($voucherEntity, $price) {
        if (empty($voucherEntity)) {
            return $price;
        }
        
        switch($voucherEntity->getField('discountMethod')) {
            case 'percent':
                $priceAfterVoucher = $price * ((100 - (int) $voucherEntity->getField('discount')) / 100);
                break;
            case 'value': 
                $priceAfterVoucher = $price - (int) $voucherEntity->getField('discount');
                break;
        }
        
        return $priceAfterVoucher;
    }
    
    public function createVoucherEntityFromForm($form) {
        $voucherEntity = new VoucherEntity();
        $companyEntity = $this->companyApi->getCompanyEntityByCompanyId($form['company']);
        $voucherEntity->setField('name', $form['name']);
        $voucherEntity->setField('code', $form['code']);
        $voucherEntity->setField('company', $companyEntity);
        $voucherEntity->setField('discount', $form['discount']);
        $voucherEntity->setField('discountMethod', $form['discountMethod']);
        $voucherEntity->setField('startDate', new \MongoDate(strtotime($form['startDate'])));
        $voucherEntity->setField('endDate', new \MongoDate(strtotime($form['endDate'])));
        $voucherEntity->setField('usageLimit', $form['usageLimit']);
        $voucherEntity->setField('status', 'active');
        $voucherEntity->setField('createdAt', new \MongoDate(time()));
        
        return $voucherEntity;
    }
    
    public function generateCode() {
        $uId1 = uniqid();
        usleep(5000);
        $uId2 = uniqid();

        $voucherPrefix = substr($uId1, -5, -2);
        $voucherSufix = substr($uId2, -4, -1);

        return strtoupper('cl_'.$voucherPrefix.$voucherSufix);
    }
    
    public function removeVoucherById($id) {
        $voucher = VoucherMapper::get($id);
        $voucher->setField('status', 'deleted');
        
        return VoucherMapper::save($voucher);
    }
}
