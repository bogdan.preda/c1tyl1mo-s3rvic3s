<?php

namespace CL\Voucher\Mapper;

use \Xeeo\Services\Database\Mongo\Mapper as Mapper;

class Voucher extends Mapper {

    protected static $collection = 'vouchers';
    protected static $entity     = '\CL\Voucher\Entity\Voucher';
}
?>
